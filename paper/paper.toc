\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Related Work}{3}{section.2}
\contentsline {section}{\numberline {3}CAN Bus}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Standards}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Startup}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Data Transmission Mechanisms}{4}{subsection.3.3}
\contentsline {section}{\numberline {4}Adversary Model}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Replay Attack}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Session Key Theft}{5}{subsection.4.2}
\contentsline {section}{\numberline {5}Cryptography}{5}{section.5}
\contentsline {subsection}{\numberline {5.1}AES-128}{5}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}CURVE25519}{5}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}X.509}{5}{subsection.5.3}
\contentsline {section}{\numberline {6}Challenges}{6}{section.6}
\contentsline {subsection}{\numberline {6.1}Key Establishment}{6}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Freshness Synchronization}{6}{subsection.6.2}
\contentsline {section}{\numberline {7}Renegotiation Scenarios}{6}{section.7}
\contentsline {subsection}{\numberline {7.1}Network Power Up}{7}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Secure Channel Membership Change}{7}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Node Freshness Out-of-Sync}{7}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Master Node Replaced}{7}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}Other Key Renegotiation Scenarios}{7}{subsection.7.5}
\contentsline {section}{\numberline {8}Detection Mechanisms}{7}{section.8}
\contentsline {subsection}{\numberline {8.1}Network Power Up}{7}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Freshness Out-of-Sync}{8}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}100 Operational Hours}{8}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Heartbeat Membership Detection}{8}{subsection.8.4}
\contentsline {subsection}{\numberline {8.5}Relink}{8}{subsection.8.5}
\contentsline {subsection}{\numberline {8.6}Missing Key Distribution}{8}{subsection.8.6}
\contentsline {section}{\numberline {9}Messages}{9}{section.9}
\contentsline {subsection}{\numberline {9.1}Using Session Key and Freshness}{9}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Message Fields}{9}{subsection.9.2}
\contentsline {section}{\numberline {10}Message Exchanges}{11}{section.10}
\contentsline {subsection}{\numberline {10.1}Key Negotiation}{11}{subsection.10.1}
\contentsline {subsubsection}{\numberline {10.1.1}Centralized}{11}{subsubsection.10.1.1}
\contentsline {subsubsection}{\numberline {10.1.2}Distributed}{14}{subsubsection.10.1.2}
\contentsline {subsection}{\numberline {10.2}Freshness Synchronization}{16}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Data}{17}{subsection.10.3}
\contentsline {subsection}{\numberline {10.4}Request Key}{18}{subsection.10.4}
\contentsline {subsubsection}{\numberline {10.4.1}Centralized}{18}{subsubsection.10.4.1}
\contentsline {subsubsection}{\numberline {10.4.2}Distributed}{19}{subsubsection.10.4.2}
\contentsline {subsection}{\numberline {10.5}Relink}{19}{subsection.10.5}
\contentsline {subsection}{\numberline {10.6}Heartbeat}{20}{subsection.10.6}
\contentsline {subsubsection}{\numberline {10.6.1}Centralized}{20}{subsubsection.10.6.1}
\contentsline {subsubsection}{\numberline {10.6.2}Distributed}{21}{subsubsection.10.6.2}
\contentsline {section}{\numberline {11}Protocol Summary}{21}{section.11}
\contentsline {subsection}{\numberline {11.1}Centralized Protocol}{21}{subsection.11.1}
\contentsline {subsubsection}{\numberline {11.1.1}Key Distribution}{22}{subsubsection.11.1.1}
\contentsline {subsubsection}{\numberline {11.1.2}Freshness Synchronization}{22}{subsubsection.11.1.2}
\contentsline {subsubsection}{\numberline {11.1.3}Other Scenarios}{22}{subsubsection.11.1.3}
\contentsline {subsection}{\numberline {11.2}Distributed Protocol}{22}{subsection.11.2}
\contentsline {subsubsection}{\numberline {11.2.1}Secure Channel Chain}{23}{subsubsection.11.2.1}
\contentsline {subsubsection}{\numberline {11.2.2}Key Distribution}{23}{subsubsection.11.2.2}
\contentsline {subsubsection}{\numberline {11.2.3}Freshness Synchronization}{23}{subsubsection.11.2.3}
\contentsline {subsubsection}{\numberline {11.2.4}Other Scenarios}{23}{subsubsection.11.2.4}
\contentsline {section}{\numberline {12}Simulation}{24}{section.12}
\contentsline {subsection}{\numberline {12.1}SocketCAN}{24}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Simulations}{24}{subsection.12.2}
\contentsline {section}{\numberline {13}Conclusion}{24}{section.13}
\contentsline {section}{\numberline {14}Results}{24}{section.14}
\contentsline {subsection}{\numberline {14.1}Protocol Correctness}{24}{subsection.14.1}
\contentsline {subsubsection}{\numberline {14.1.1}Detection Mechanisms}{24}{subsubsection.14.1.1}
\contentsline {subsubsection}{\numberline {14.1.2}Message Exchange Correctness}{25}{subsubsection.14.1.2}
\contentsline {subsubsection}{\numberline {14.1.3}Message Exchange Security}{25}{subsubsection.14.1.3}
\contentsline {subsubsection}{\numberline {14.1.4}Simulations}{25}{subsubsection.14.1.4}
\contentsline {subsection}{\numberline {14.2}Bandwidth Utilization}{25}{subsection.14.2}
\contentsline {subsection}{\numberline {14.3}Traffic Burst}{25}{subsection.14.3}
\contentsline {subsection}{\numberline {14.4}Practicality}{25}{subsection.14.4}
\contentsline {subsection}{\numberline {14.5}Attack Resistance}{25}{subsection.14.5}
\contentsline {subsubsection}{\numberline {14.5.1}Replay Attack}{26}{subsubsection.14.5.1}
\contentsline {subsubsection}{\numberline {14.5.2}Session Key Theft}{26}{subsubsection.14.5.2}
\contentsline {section}{\numberline {15}Conclusion}{26}{section.15}
