\contentsline {section}{\numberline {1}Introduction}{3}{section.0.1}
\contentsline {section}{\numberline {2}Related Work}{3}{section.0.2}
\contentsline {section}{\numberline {3}CAN Bus}{4}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Standards}{5}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}Startup}{5}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}Data Transmission Mechanisms}{5}{subsection.0.3.3}
\contentsline {section}{\numberline {4}Adversary Model}{5}{section.0.4}
\contentsline {subsection}{\numberline {4.1}Replay Attack}{5}{subsection.0.4.1}
\contentsline {subsection}{\numberline {4.2}Session Key Theft}{6}{subsection.0.4.2}
\contentsline {section}{\numberline {5}Cryptography}{6}{section.0.5}
\contentsline {subsection}{\numberline {5.1}AES-128}{6}{subsection.0.5.1}
\contentsline {subsection}{\numberline {5.2}CURVE25519}{6}{subsection.0.5.2}
\contentsline {subsection}{\numberline {5.3}X.509}{6}{subsection.0.5.3}
\contentsline {section}{\numberline {6}Challenges}{7}{section.0.6}
\contentsline {subsection}{\numberline {6.1}Key Establishment}{7}{subsection.0.6.1}
\contentsline {subsection}{\numberline {6.2}Freshness Synchronization}{7}{subsection.0.6.2}
\contentsline {section}{\numberline {7}Renegotiation Scenarios}{8}{section.0.7}
\contentsline {subsection}{\numberline {7.1}Network Power Up}{8}{subsection.0.7.1}
\contentsline {subsection}{\numberline {7.2}Secure Channel Membership Change}{8}{subsection.0.7.2}
\contentsline {subsection}{\numberline {7.3}Node Freshness Out-of-Sync}{8}{subsection.0.7.3}
\contentsline {subsection}{\numberline {7.4}Master Node Replaced}{9}{subsection.0.7.4}
\contentsline {subsection}{\numberline {7.5}Other Key Renegotiation Scenarios}{9}{subsection.0.7.5}
\contentsline {section}{\numberline {8}Detection Mechanisms}{9}{section.0.8}
\contentsline {subsection}{\numberline {8.1}Network Power Up}{9}{subsection.0.8.1}
\contentsline {subsection}{\numberline {8.2}Freshness Out-of-Sync}{9}{subsection.0.8.2}
\contentsline {subsection}{\numberline {8.3}100 Operational Hours}{10}{subsection.0.8.3}
\contentsline {subsection}{\numberline {8.4}Heartbeat Membership Detection}{10}{subsection.0.8.4}
\contentsline {subsection}{\numberline {8.5}Relink}{10}{subsection.0.8.5}
\contentsline {subsection}{\numberline {8.6}Missing Key Distribution}{10}{subsection.0.8.6}
\contentsline {section}{\numberline {9}Messages}{10}{section.0.9}
\contentsline {subsection}{\numberline {9.1}Using Session Key and Freshness}{11}{subsection.0.9.1}
\contentsline {subsection}{\numberline {9.2}Message Fields}{11}{subsection.0.9.2}
\contentsline {section}{\numberline {10}Message Exchanges}{14}{section.0.10}
\contentsline {subsection}{\numberline {10.1}Key Negotiation}{14}{subsection.0.10.1}
\contentsline {subsubsection}{Centralized}{14}{section*.2}
\contentsline {subsubsection}{Distributed}{17}{section*.3}
\contentsline {subsection}{\numberline {10.2}Freshness Synchronization}{21}{subsection.0.10.2}
\contentsline {subsection}{\numberline {10.3}Data}{22}{subsection.0.10.3}
\contentsline {subsection}{\numberline {10.4}Request Key}{23}{subsection.0.10.4}
\contentsline {subsubsection}{Centralized}{23}{section*.4}
\contentsline {subsubsection}{Distributed}{24}{section*.5}
\contentsline {subsection}{\numberline {10.5}Relink}{25}{subsection.0.10.5}
\contentsline {subsection}{\numberline {10.6}Heartbeat}{26}{subsection.0.10.6}
\contentsline {subsubsection}{Centralized}{26}{section*.6}
\contentsline {subsubsection}{Distributed}{27}{section*.7}
\contentsline {section}{\numberline {11}Protocol Summary}{28}{section.0.11}
\contentsline {subsection}{\numberline {11.1}Centralized Protocol}{28}{subsection.0.11.1}
\contentsline {subsubsection}{Key Distribution}{29}{section*.8}
\contentsline {subsubsection}{Freshness Synchronization}{29}{section*.9}
\contentsline {subsubsection}{Other Scenarios}{29}{section*.10}
\contentsline {subsection}{\numberline {11.2}Distributed Protocol}{29}{subsection.0.11.2}
\contentsline {subsubsection}{Secure Channel Chain}{29}{section*.11}
\contentsline {subsubsection}{Key Distribution}{30}{section*.12}
\contentsline {subsubsection}{Freshness Synchronization}{31}{section*.13}
\contentsline {subsubsection}{Other Scenarios}{32}{section*.14}
\contentsline {section}{\numberline {12}Simulation}{32}{section.0.12}
\contentsline {subsection}{\numberline {12.1}SocketCAN}{32}{subsection.0.12.1}
\contentsline {subsection}{\numberline {12.2}Simulations}{32}{subsection.0.12.2}
\contentsline {section}{\numberline {13}Conclusion}{32}{section.0.13}
\contentsline {section}{Appendices}{35}{chapter*.15}
\contentsline {section}{\numberline {A}Simulation Code}{35}{section.Alph0.1}
\contentsline {subsection}{\numberline {A.1}main}{35}{subsection.Alph0.1.1}
\contentsline {subsection}{\numberline {A.2}Simulation}{37}{subsection.Alph0.1.2}
\contentsline {subsection}{\numberline {A.3}Centralized Protocol}{39}{subsection.Alph0.1.3}
\contentsline {subsubsection}{Generic}{39}{section*.16}
\contentsline {subsubsection}{Master}{42}{section*.17}
\contentsline {subsection}{\numberline {A.4}Distributed Protocol}{46}{subsection.Alph0.1.4}
