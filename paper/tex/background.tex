\section{CAN Bus}
% Describe a CAN bus
Figure~\ref{fig-message-transmission} shows nodes connected on a CAN bus.
Connections utilize two shared wires terminated by resistors on both ends.
Only one node may broadcast a message at a time.
This message is seen by all nodes.

% how does arbitration occur
Bus arbitration determines which node is allowed to transmit;
it occurs prior to message transmission.
Nodes broadcast an ID number.
The lowest ID is awarded the bus.

% significance of CAN-FD over CAN
This work relies on the larger data field allowed by CAN-FD.
CAN 2.0 communications maintain the same bus speed throughout message transmission.
However, CAN-FD communications increase the transmission rate after arbitration.
During arbitration multiple nodes may be broadcasting IDs.
Transmitted bits must be allowed to propagate the entire length of the bus;
this allows nodes with higher IDs to receive the lower (dominant) IDs and stop transmitting.
Thus, arbitration requires a slower transmission rate.

\subsection{Standards}
Four relevant standards exist:
(1) Bosch CAN 2.0,
(2) SAE J1939,
(3) ISO 11783, and
(4) CAN-FD.
CAN 2.0 and CAN-FD define CAN architecture and data frame formats.
CAN-FD enhances CAN 2.0 to allow for larger data frames and variable bus speeds.
SAE J1939 standardizes identifier bit meanings, physical wiring, baud rate, and network management.
ISO 11783 extends SAE J1939 to off-road vehicles;
this standardizes more specific applications such as diagnostic services.

\subsection{Startup}
% describe messages sent by all nodes each time the bus starts up
ISO 11783 requires each ECU to obtain a unique NAME.
NAME is a 64-bit identity negotiated at network startup.
8-bits of this identity become the source address (SA) for the ECU.
{\it The SA is unique for each ECU.}
We associate a freshness value with each node using SA as a proxy for node identity.

Four different messages may be sent by ECUs in the process of claiming an identity:
(1) request for address claimed,
(2) address claimed,
(3) cannot claim source address, and
(4) commanded address.
At startup, each ECU sends (1);
this causes all other ECU to broadcast their claimed SA.
The ECU will broadcast (2) containing an SA different from response messages.
The ECU will prefer its previous SA stored in non-volatile memory.

\subsection{Data Transmission Mechanisms}
% describe network layer protocols
% what is the behavior of the nodes in each case
%
% single message 
%	without src, destination
%	with src, destination
The SAE J1939 standard defines how to interpret CAN identifier bits.
Identifier bits are used for arbitration;
they also encode information about
(1) message data and optionally, (2) source, destination addresses.
A subset of the identifier bits are interpreted as a Parameter Group Number (PGN).
The PGN identifies the message type;
message data is interpreted accordingly.
For certain PGNs, identifier bits contain source, destination addresses.
However, in practice the source, destination address version is rarely used.

% several messages -- BAM messages
A multi-packet data transfer protocol is defined by SAE J1939.
It supports destination-specific and globally-targeted multi-packet messages.
The global message protocol, Broadcast Announce Message (BAM), uses two message types:
(1) transport protocol connection management,
(2) transport protocol data transfer.
Transport protocol messages are an efficient way to distribute large amounts of information.
Our PKI uses 4KB X.509 certificates; these are distributed using BAM messages.
%A transfer may be addressed to a specific ECU or all ECUs;
%this affects the receiver response and timing requirements.


\section{Adversary Model}
% WHO are we trying to prevent from doing WHAT?
% what are the capabilities of the adversary?
%
% an adversary who has compromised a node part of the secure channel
%
% an adversary who has compromised a node NOT part of the secure channel
%
% an adversary who has attached a node to the bus
This work's purpose is to prevent an adversary from performing any meaningful bus communication.
This section provides a description of each attack type.
%TODO
%The protocol is shown to work against such attacks in Section~\ref{attack-resistance}.

% describe the adversary in greater detail
An adversary may function as a bus {\it hacker} or key {\it thief}.
A {\it hacker} tries to gain system control by sending messages from a compromised node;
it is assumed such a node does not know the current session key.
A {\it thief} attempts to compromise valuable private data.
Here, the current session key is of concern.
It is valuable because it enables sending messages on the bus.
A stolen session key might be embedded in some third-party equipment;
it could then perform illegitimate bus communications.

\subsection{Replay Attack}
% why do messages need to be protected?
Data and control messages travel on the CAN bus.
Sensors generate data messages;
some of these data messages may determine future control messages.
Control messages effect actions performed by an actuator.
Thus, both data and control messages must not be erroneous.

% how does the attack work?
An adversary performing a replay attack observes bus messages and subsequent associated actions.
The messages are then injected at a later time in hopes of eliciting the same action.
%
% the danger of this attack
Many CAN implementations currently offer no protection against such attacks.
Replay attacks are both easy to perform and high impact;
full system control is possible.
Therefore, this constitutes a safety-critical vulnerability.

% how can it be prevented
This attack can be prevented by embedding a notion of time in messages.
The adversary must not be able to modify this time.
Nodes can be programmed to respond only to timely messages;
replayed messages will be discarded.

\subsection{Session Key Theft}
The session key is the basis for message integrity.
Replay attack resistance relies on the session key remaining private.
A third party may make use of a stolen session key to make their equipment operable (and sell-able).
Active third-party equipment presents concerns for the Original Equipment Manufacturer (OEM);
liability for malfunctioning third-party equipment is possible.

In our solution, bus nodes establish a session key.
Several bus scenarios present opportunities for an adversary to learn the key.
We detail these scenarios in Section~\ref{sec-renegotiation-scenario-detection}.
Detecting such a scenario invokes session key re-negotiation.


% describe the cryptographic tools we will utilize
\section{Cryptography}
\label{sec-crypto}
\subsection{AES-128}
AES-$128$ is a symmetric block cipher with $128$-bit keys and $128$ bit block size.
AES-$128$ is very fast compared to asymmetric cryptography.
It quickly signs messages after session key establishment.
This signature is a keyed hash appended to a message;
it contains a freshness counter value to guarantee timeliness.

\subsection{CURVE25519}
CURVE25519 is an Elliptic Curve Diffie Hellman Exchange (ECDHE) function \cite{bernstein2006curve25519}.
It establishes a shared secret over an insecure channel.
Two parties ($N$) are involved each having a public key ($e_N$) and private key ($d_N$). 
These keys are paired; two parties exchanging $e_N$ can compute a shared secret using their $d_N$.
%Party $N$ has (public key, private key) pair ($e_N$, $d_N$).
This exchange generates a symmetric secret ($K$).
The specific computations are as follows.
Consider two parties A,B.
Each generates public key 
$e_n=CURVE25519(d_n,\underline{9})$;
\underline{9} is a public string.
Public keys are exchanged.
Each party generates the shared secret 
$K=CURVE25519(d_A,e_B)=CURVE25519(d_B,e_A)$.
HASH($K$) is used for message encryption and authentication.

% this is something that should be described later
%The first two ECU use ECDHE to create a session key.
%These ECU can then interrogate other nodes for proof they belong to the vehicle.
%Upon verifying the nodes these ECU can send the session key and freshness.

\subsection{X.509}
X.509 is a certificate format standard.
Certificates are used to communicate public keys with authenticity and integrity.
Placing a Trusted Third Party (TTP) signature in the certificate guarantees these security goals.
Parties verify the signature using the TTP's public key.

{\it We assume each ECU can both parse X.509 certificates and has a certificate signed by a TTP.}
An ECU can be interrogated for proof it belongs to a vehicle;
in response it will present its certificate.
This certificate contains the public key for the ECU.

Exchanging certificates establishes a secure channel between two ECUs.
Each has the others public key and can generate a shared CURVE25519 secret.
The private key associated with the certificates is required to generate this secret.
This guarantees source node authenticity; 
each node must have the private key associated with the certificate it exchanged.

%
%The interrogating ECU can verify the certificate has authenticity and integrity because it knows RA's public key.
%\textit{(Assume all nodes have a self-signed certificate by RA to get its public key.)}
%
%NOT TRUE
%The interrogating ECU can then encrypt the session key with the interrogated node's public key
%and broadcast it on the bus.
%The interrogated node then decrypts this message with its private key obtaining the session key.


%A similar approach could be used to communicate freshness.
%The interrogating node can then encrypt freshness with the session key and broadcast.
%This allows all nodes to resync freshness. 


