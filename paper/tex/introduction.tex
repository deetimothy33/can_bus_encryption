% 0. background.
% 1. what is the problem?
% 2. what would a solution look like?
\section{Introduction}
% background
Typical CAN bus applications have real-time requirements.
Data and control messages with hard deadlines are transmitted.
In vehicular applications, control messages frequently activate actuators.
Thus, integrity and authenticity of control messages is critical for system safety.

CAN bus bandwidth is at a premium.
In the past, CAN standards \cite{iso11898} allowed bus bandwidth of $1 Mbps$.
Functional requirements took precedence over security concerns.
However, many CAN networks operated in isolated environments unconcerned integrity and authenticity.
Now, network access points to these networks are increasingly common.
Thus, the probability of attack is increased.

Network attack vectors require protocol-level security.
Compromised nodes can read all bus traffic and submit arbitrary packets.
Without message integrity and authenticity adversaries gain complete bus control using
replay attacks and reverse engineered protocols.
Message authenticity requires source node authenticity verification.

\iffalse
% appearance of solution
{\it The three attributes} any solution must have are integrity, authenticity, and low bandwidth overhead.
Message integrity ensures modifications to messages are detectable.
Solutions must guarantee a receiver does not consume a corrupted message.
Message authenticity provides confidence a message source is legitimate.
{\it The two requirements} for authenticity are node authentication and exposing the sending node in messages.
Node authentication requires the verification of node identity.
%Exposing the sending node in messages is necessary for the receiver to validate authenticity.
Verifying message authenticity requires embedding information known only to authenticated nodes in each message.
Low bandwidth overhead requires solutions imposing few $\frac{\textrm{messages}}{\textrm{unit time}}$.
They should also minimize traffic bursts;
these represent the worst case execution time (WCET).
Real-time systems must be certified based on WCET.
Therefore, feasibility depends on $MAX(\frac{\textrm{messages}}{\textrm{unit time}})$ at some granularity
($\frac{\textrm{messages}}{1000 ms}$, $\frac{\textrm{messages}}{100 ms}$, ...).
An ideal solution is feasible at the smallest possible granularity.
Thus, {\it the two considerations} are overhead and granularity.
\fi

Public Key Infrastructure (PKI) allows secure channel establishment.
Each party has public key ($e_n$) and a private key ($d_n$).
An exchange, such as that in Elliptic Curve Cryptography (ECC), generates a symmetric key using $e_n$ and $d_n$.
Introducing a trusted third party (TTP) enables source node authenticity verification.
A {\it certificate} containing each party's public key is signed by the TTP using its private key.
All parties have a self-signed TTP certificate providing the TTP's public key.
The TTP's public key can be used to verify certificates;
the signature contained therein could only have been generated using the TTP private key;
only the TTP can produce valid certificates.
Nodes must have the private key corresponding to the public key in a certificate to make use of it.
An exchange to generate a symmetric key requires decrypting messages using this private key.
Thus, if a node is able to complete the exchange then it must be authentic.

% enabling technologies (for solution)
CAN-FD is a new CAN bus communication standard \cite{iso11898}.
Bit transmission rate is increased after arbitration;
this allows for larger data frames.
Old CAN standards allow up to $8$ bytes of data while
CAN-FD allows up to $64$.
This work uses a small number of these additional bits along with PKI to provide
(1) message integrity, (2) message authentication, and (3) source node authenticity.
ECC is used to communicate a session key.
This key generates a Message Authentication Code (MAC) appended to each message.

Message integrity requires timely unmodified message content.
A MAC can ensure both.
Making the MAC a keyed hash function of message 'time' and content allows the receiver to verify both.
A freshness value constitutes the message 'time'.
The challenge is to synchronize the expected freshness value between sender and receiver.
Messages originating from a particular node appear on the bus in a deterministic order.
Tying the freshness value to this order provides a reliable notion of freshness.
However, one freshness counter must be maintained for each node on the bus.

% what is our solution
This paper presents centralized and distributed protocols for key distribution and freshness synchronization.
Section~\ref{sec-challenges} elucidates the challenges thereof.
Protocols are successful if they inhibit meaningful adversary communication on the CAN bus.
Section~\ref{sec-renegotiation-scenario-detection} provides descriptions scenarios requiring key renegotiation.
Section~\ref{sec-detection-mechanisms} describes detection of such scenarios.
Section~\ref{sec-message-exchanges} describes message exchanges in response to detected scenarios
using frame formats provided in Section~\ref{sec-frame-format}.
Section~\ref{sec-protocols} describes protocols as a set of tuples;
each contains {\it (scenario, detection mechanism, message exchange)}.
