%%%
% PROBLEM DESCRIPTION
%%%
% these first three sections present the problem


\section{Challenges}
\label{sec-challenges}

\subsection{Key Establishment}
% use of public key infrastructure ( authentication )
Only authentic nodes may join the secure channel and receive the session key.
Public Key Infrastructure (PKI) is used to guarantee source node authenticity.
Each node is given an certificate at birth.
The certificate ties the node to a specific vehicle;
it is signed by a Trusted Third Party (TTP).
TTP's public key is known by all nodes allowing them to verify these certificates.
Nodes can exchange certificates to establish a shared secret.
This secret is generated using Elliptic Curve Diffie-Hellman Exchange (ECDHE) using CRUVE25519;
described at length in Section~\ref{sec-crypto}.

% establishing a key
The challenge is to use these exchanges to establish a session key known by all secure channel nodes.
A centralized and distributed protocol are proposed.
In the centralized protocol, a master node establishes a shared secret with each other node.
The master generates then a session key and uses the established shared secrets to distribute it to each node.
Section~\ref{sec-protocols} provides detailed protocol descriptions.

% describe what the key allows once it is established
% -- happens in Message Fields Section --

% distribution is expensive
%Transmission of $Encrypt(P_x,S_n)$ is required for each secure channel node.
%If a certificate exchange and subsequent key distribution occurs for each node individually
%then distribution is expensive.
%Handling distribution efficiently is a challenge.

\subsection{Freshness Synchronization}
\begin{figure}
	\centering
	\includegraphics[width=0.5\hsize]{message_transmission.pdf}
	\caption{
		Application software on each node stores a freshness counter, message data, and session key.
		The counter value is appended to the message data;
		a keyed hash is applied to this value using the session key.
		The resultant $16$ byte message is placed into a hardware queue.
		The node's transceiver will attempt to send the message until it is successful.
	}
	\label{fig-message-transmission}
\end{figure}

% why does a counter make sense
Nodes require a consistent notion of freshness.
Inconsistent freshness values between sender and receiver render message integrity unverifiable.
The CAN bus is a broadcast medium;
each node sees every message.
Thus, a counter reflecting the number of successfully transmitted messages can be used for freshness.
However, transceivers introduce delays in sending messages as described in Figure~\ref{fig-message-transmission}.
A node must win bus arbitration before it can send a message.
Messages can be sent by other nodes in the interim.
Freshness counters must not become outdated during this time.
The queued messages will become useless otherwise.
The challenge is to maintain counter consistency among nodes in light of non-deterministic bus behavior.

%one could imagine a freshness that is a more complex function of MSB(COUNTER) and LSB(COUNTER)
%the advantage is it would make the freshness value more difficult to guess
%the downside is it would be more difficult for the receiver to verify

% a freshness could be defined to be a small number of bits (say 8)
% the entire message counter could be used in generating the message MAC
% then in some sense [n..8] for the freshness becomes a secret.
% this would enable a larger MAC => more security
% the difficult is in the receiver knowing the counter value.

% make sure to say the benefit of doing it this way is that the only out-of-sync condition is watchdogging
% how is freshness defined
We maintain a table of $8$-bit counters $C$ at each node.
$C$ contains one entry $C_i$ for each secure channel node $i$.
$C_i$ is a count of messages successfully transmitted by node $i$.
Each source address is associated with an entry in $C$.
Node source addresses are a proxy for their identity.
Including source addresses in the message authentication code allows identify verification.
Each $C_i$ begins at $0$ when a session key is established.

%Freshness $f$ is $LSB(C)$.
%$LSB(C)$ provides the $4$ least significant bytes of $C$.

% (2) how can nodes detect when their counter is out-of-sync
Detecting whether $C_i$ is in-sync with the network is necessary.
Out-of-sync $C_i$ causes messages to be rejected.
Section~\ref{sec-detection-mechanisms} describes detection of this scenario.
% (3) how can nodes resynchronize their counter
Out-of-sync $C_i$ values must be resynchronized.
Section~\ref{sec-message-exchanges} provides algorithms for accomplishing this.
%Section~\ref{sec-protocols} provides centralized and distributed methods.

Nodes become out-of-sync if they are offline during bus operation;
this is the only situation.
The node will fail to increment its freshness counters while it is offline.


\section{Renegotiation Scenarios}
\label{sec-renegotiation-scenario-detection}
% describe
% 	what the scenario is
% 	what we do about it (after it is detected)

% discuss 
% (1) bus events requiring 
%	(a) session key renegotiation
%	(b) freshness value resynchronization
% for each bus event
% (2) problems in detecting the bus event

The adversary must be prevented from producing an apparently legitimate message.
Producing such a message requires the session key and freshness values.
Some bus events indicate one of these values is vulnerable.
Responding to these events requires value renegotiation.

% key renegotiation: when, how
Session key negotiation occurs when
(1) there is no existing session key or
(2) secure channel membership changes.
% freshness sync: when, how
Freshness synchronization occurs whenever a node falls out-of-sync.
Freshness counter values start at $0$ when a session key is established.
Freshness should not be reset to the same value each time the vehicle starts up.
This exposes the bus to replay attacks because the session key is not necessarily changed.
For example, suppose all counter values start at $0$ at power up and the session key is not renegotiated.
The messages from the previous power cycle will have valid freshness values and signatures.
An adversary recording such messages can replay them.
For this reason, counter values are assumed to persist across power cycles.
This alleviates the need to synchronize freshness at power up events where the session key is not changed.

Persistent memory is already required to store the session key.
The existence of such memory poses the largest cost.
The additional cost of freshness table storage is negligible.
However, persistent memory has a limited number of write cycles.
For this reason, the table should only be written on a graceful power-down.
The key can be renegotiated at power up if no freshness table exists.

This section exhaustively enumerates possible scenarios.
Each subsection corresponds to a scenario.
Each scenario is defined.
How our protocols handle it is briefly described.
Section~\ref{sec-detection-mechanisms} details how these situations are detected.
Section~\ref{sec-message-exchanges} describes the messages exchanged upon detection.
%Event detection and influence on the session key ($S_v$) and freshness table ($C$) are discussed.

% 1
\subsection{Network Power Up}
\label{sec-network-power-up}
Key negotiation should happen after all nodes are powered on.
Otherwise each node modifies secure channel membership as it comes online trigging key renegotiation.
Thus, detecting when all nodes are powered on is useful.
At minimum, protocols must account for disparate node startup times.
%Establishing a key before all nodes are online results in an immediate secure channel membership change forcing key renegotiation.
%This occurs when offline nodes come online.
Our protocols forestall key negotiation as long as possible allowing time for nodes to start up.

% 2, 3
\subsection{Secure Channel Membership Change}
\label{sec-secure-channel-membership-change}
The session key must be renegotiated when secure channel membership changes.
Adding or removing a node from an established secure channel constitutes a membership change.
The added node should not be able to decrypt old messages.
Therefore, the previous session key must not be exposed to it.
Removed nodes may be attacked offline.
Offline attacks may be stronger than online attacks.
Renegotiating in this scenario negates their effectiveness.

The challenge is how to detect secure channel membership change.
Added nodes announce their presence on the bus.
For removed nodes, A notion of node aliveness is kept using bus traffic.

% 4, 5
\subsection{Node Freshness Out-of-Sync}
\label{sec-node-freshness-out-of-sync}
Node freshness tables become out-of-sync when a node is offline during transmissions.
This may occur when 
(1) a node unexpectedly restarts or
(2) the network is powered up without secure channel membership change.
In (2) the session key is not renegotiated.
Some nodes may begin transmitting messages before all nodes are powered on.
The offline nodes fail to increment the corresponding counters leading to an out-of-sync state.

We provide a message allowing nodes to request freshness synchronization.
A single message requests synchronization of all out-of-sync counters.
It is a function of freshness table counter values.
The receiver responds with one message for each out-of-sync counter.
In both (1) and (2) many freshness counters are likely to become out-of-sync.
Using a single request message alleviates the necessity to make requests for each entry in the table.

% How to give a node and opportunity to re-sync its counter
%Benign programming errors or adversary interference may cause erroneous node freshness.
%Nodes are given opportunity to resynchronize freshness values.
%Resynchronization should be possible for authentic nodes.
%Such a node will possess $S_n$.

% the question is then how to verify a node has Sv
% SYNC_ASK message containing data[8..15]=HASH(VALUE,S_n) with data[0..7]=VALUE

% 6
\subsection{Master Node Replaced}
\label{sec-master-node-replaced}
Typical applications employ a computationally powerful master node.
Cost reduction is achieved in most cases because the majority of nodes are less computationally demanding and therefore cheaper.
We assume this use case.
Hence, A new master node will know that it is the master node.
The new master sends a message when it comes online.
Other nodes begin session key negotiation in response.

%Replacing the master node requires
%(1) choosing a new master node.
%Secure channel membership modified;
%(2) The session key must be renegotiated.

% 7
\subsection{Other Key Renegotiation Scenarios}
\label{sec-other-key-renegotiation-scenarios}
The key is renegotiated after 100 hours of operation.
This places an upper bound on the amount of time an adversary has to attack a key.
We utilize a notion of time based bus messages to detect this condition.


\section{Detection Mechanisms}
\label{sec-detection-mechanisms}
% several protocols can use the same detection mechanisms
% describe
% 	What is detected
% 	How we implement its 

Detecting key renegotiation and freshness synchronization scenarios is the objective.
%Protocols require detection of security-relevant bus events.
Event detection triggers protocol-dependant message exchanges described in Section~\ref{sec-message-exchanges}.

This section describes bus event detection mechanisms.
Each subsection corresponds to one detection mechanism.

% how to know when all nodes have come online?
\subsection{Network Power Up}
\label{sec-detect-network-power-up}
Session key negotiation should be delayed until all secure channel members are powered on.
%Nodes turning on transmit join request message.
%Each new join request message resets a timer on each node.
%The timer is reset to a value larger than the maximum possible interval between node awakenings;
%it decrements at some frequency.
%All nodes are considered powered on when the timer reaches $0$.

The centralized protocol handles this situation by delaying actions required for all nodes.
The master will collect certificates from nodes as they come online.
The hope is the time taken to gather all certificates affords enough time for all nodes to come online.
The master certificate is then distributed using a BAM to all nodes simultaneously.
The session key is then generated and distributed.

The distributed protocol works in an incremental fashion.
Nodes will likely have time to power up while other nodes are performing initial communications
As a result, key negotiation does not happen until all nodes are supposed to have come online.
Additionally, the incremental process makes adding a node inexpensive.

An alternative approach might allows a time window in which a session key may be distributed.
The window needs to be long enough to accommodate time for all nodes to power up and negotiate a session key.

% how does a node detect when it is out of sync
\subsection{Freshness Out-of-Sync}
\label{sec-detect-self-out-of-sync}
% invalid $C$ status detection mechanisms
A subset of a node's freshness counter is included in the security bits appended to each message.
$f_i=LSB(C_i)$ where $LSB()$ is the least significant $4$ bytes.
Nodes observe $f_i$ and consider themselves out-of-sync if $f_i$ does not match $C_i$.

% how to fix out-of-sync condition once detected
The first remedial action taken by an out-of-sync node is to set $LSB(C_i)$ equal to $f$ from the message.
If $f$ is less than $LSB(C_i)$, then the node will increment the $9^{th}$ bit of $C_i$ by $1$.
The node will know its $C_i$ is correct because $HASH(message||C_i,S_n)$ will equal the value included in the message.
$S_n$ is the session key.
Failure of this method means the $C_i$'s most significant $4$ bytes are unknown.
A message is sent to request synchronization.
The response to this message is taken as the new freshness value.
In the centralized protocol, the master node responds to this message.
In the distributed protocol the node which generated the session key assumes this responsibility.

%A large number of messages failing to match $f$ maintained at watching node indicates the node is out-of-sync.

% This is from SINGLE FRESHNESS VALUE -- NOT TABLE VERSION
% define the protocol more precisely
%Nodes keep a buffer ($B$) of size $n$.
%Transmitted message freshness values are compared to $f$ maintained at the node.
%Matching $f$ results in $0$ shifted into the buffer;
%$1$ is shifted in otherwise.
%The number of $1$'s in the buffer ($ONES(B)$) is the fraction of the previous $n$ messages where $f$ did not match.

%Nodes consider themselves out-of-sync when $\frac{ONES(B)}{n} > 0.5$.

\subsection{100 Operational Hours}
\label{sec-detect-operation-hours}
Two methods are provided for detecting 100 operational hours.
The first method leverages the ENGINE HOURS message.
This message is sent by the engine.
The master node in the centralized protocol records the engine hours when a secure channel is established.
In the distributed protocol, the node generating the session key assumes this responsibility.
Key renegotiation is initiated when the ENGINE HOURS message indicates 100 hours have passed.
A potential problem with this method is the assumption that the ENGINE HOURS message has integrity.
In practice, the engine may not be using the newer CAN protocols for some time.
An adversary could artificially trigger key renegotiation with an erroneous ENGINE HOURS message.

% how to detect when the bus has been in operation for 100 hours
An alternative method estimates 100 hours of operation using bus load and number of messages sent.
$C$ is the freshness table maintained by all nodes on the network.
$C_i$ is the freshness counter for a particular node.
The sum of freshness counters, $\Sigma_0^{|C|-1} C_i$, is the number of messages sent since secure channel creation.
It increments at some frequency, $\lambda \frac{\textrm{messages}}{\textrm{sec}}$.
$100$ hours is $360000$ seconds.
Thus, $360000 \times \lambda$ is the value corresponding to 100 operational hours.
When this value equals the sum of freshness counters key renegotiation is initiated.

% software-level solution for
% detecting secure channel membership change
\subsection{Heartbeat Membership Detection}
\label{sec-detect-heartbeat-membership-detection}
Detection of node failure and bus membership can be seen as equivalent problems.
\cite{rufino2003node} provides an approach to node failure detection;
it is described briefly in Section~\ref{sec-related-work}.
Adapting this algorithm solves the secure channel membership change detection problem.
The centralized protocol master node maintains a notion of aliveness for each node.
%In the distributed protocol, the node generating the session key assumes this responsibility.
In the distributed protocol, there is a notion of logical neighbors.
Neighbors track each-other's aliveness.

Node aliveness is depends on bus traffic.
Nodes are expected to produce messages at some frequency.
If a node fails to do so, 
then a message is sent to elicit a response.
Failure to respond to this message constitutes node removal.

\subsection{Relink}
\label{sec-detect-relink}
The distributed protocol has a notion of neighboring nodes.
Each node maintains two logical neighbors.
The session key is propagated from neighbor to neighbor during key distribution.
\nameref{sec-detect-heartbeat-membership-detection} detects node removal.
Disconnected neighbors attempt to establish a secure channel using a RELINK message.
All disconnected neighbors send this message.
If two RELINK messages are observed, then the sending nodes complete an exchange.
This maintains the structure of logical neighbors.

\subsection{Missing Key Distribution}
\label{sec-detect-missing-key-distribution}
A node may miss key distribution.
An unexpected restart may cause this to occur.
Nodes transmit the REQUEST\_KEY message to force KEY retransmission.
In the centralized protocol, this request is sent to the master.
In the distributed protocol, this is sent to a logical neighbor.
Nodes set a bit in persistent memory when a new key is being negotiated.
This bit is cleared after the new session key is saved to persistent memory.
REQUEST\_KEY is sent when the bit is not clear at power up.


%CAN Frame Types
%data frame
%remote frame
%error frame
%overload frame
%\url{http://www.esd-electronics-usa.com/CAN-Remote-Frames.html}
%
% discuss that only a subset of the counter bits are included in the frame
%
% distinguish among different frame formats
% include a figure and description for each
% 1. message frames
% 2. certificate-sending frames
% 3. key distribution frames
% 4. freshness synchronization frames
\section{Messages}
\label{sec-frame-format}

\input{tex/protocol/protocol_frame.tex}

CAN messages are defined for key establishment and freshness synchronization.
Table~\ref{fig-state-message} provides a complete list of required messages.
Centralized and distributed protocol use the same messages.
However, some messages have modified meanings for each protocol.
Some messages handle special situations.

This section describes how the session key and freshness value provide
(1) message integrity, (2) message authentication, and (3) source node authenticity.

% describe how the Session key and Freshness value
%	provide source node authenticity, message authenticity, and message integrity
% describe MAC generation
\subsection{Using Session Key and Freshness}
This key is is used to generate a Message Authentication Code (MAC) appended to each DATA message.
The $MAC$ is $LSB(C_i) \| HASH(m \| C_i \| SA, S_n)$ where
$C_i$ is a node's freshness counter,
$LSB(C_i)$ is the least significant $4$ bytes of $C_i$,
$\|$ is append,
$HASH$ is a keyed hash function,
$m$ is the message,
$S_n$ is the session key, and
$SA$ is the node's source address.
The source address is used to index the freshness counter table; 
the corresponding value is incremented for successfully transmitted messages.
Therefore, the node source address is included in the hash.
This guarantees its integrity.
The receiving will receive the source address as part of the identifier.

% describe why each thing is included in the mac
% how does it provide for one of the security goals:
% 	source node authenticity, message authenticity, and/or message integrity
The keyed hash can only be generated if the session key and freshness values are known.
The receiver verifies these values are known to the sender by generating the hash.
If the generated hash matches the hash included in the message,
then the receiver knows the sender must have know the session key and freshness values.
Only authentic source nodes can acquire the session key.
Thus, source node authenticity is provided.
A matching hash also verifies all values included in the hash have integrity.
The message, freshness counter, and source address are included in the hash.

% describe the number of bits used for each thing in each message
% describe the messages found in messagetable
% 	why do they exist
% 	what are they used for
% 	how are they used
% 	do they differ between centralized and distributed protocols?
\subsection{Message Fields}
% messages table
\input{tex/protocol/messagetable.tex}

This section describes the messages and data formats used by \nameref{sec-message-exchanges}, 
Section~\ref{sec-message-exchanges}.
Table~\ref{fig-state-message} enumerates all protocol messages along with their meaning and content.
Figure~\ref{fig-protocol-frame} depicts message bit fields.
Words in ALL CAPS refer to message types.
Normal message transmissions utilize DATA messages.
Other message types are used for key negotiation and freshness synchronization.

%The message is $16$ bytes in total;
%this has equivalent CAN-FD transmission time to a standard $8$ byte CAN message.
%The tag is a keyed hash of freshness appended to data.
%The receiver verifies the keyed hash ensuring data and freshness are not modified.
%DATA message format is the default unless otherwise specified.
%Freshness and tag provide integrity and authenticity.

%
% Key negotiation
%

% describe the message table
Centralized and distributed protocols use JOIN\_ASK messages in different ways.
In the centralized protocol, it is a request {\it by} a node to join the secure channel.
Nodes request the master node to initiate the CERTIFICATE exchange.
In the distributed protocol, it is a request {\it to} a node to join the secure channel.
Nodes incrementally establish the secure channel.
The last node joined to the secure channel sends a JOIN\_ASK message.
This initiates a CERTIFICATE exchange to join an unjoined node.
Including a random number $r$ and $H(r,S_n)$ allows current session key validation.
If the session key is current,
then the response is an OK message
and exchanging CERTIFICATE messages is avoided.

%Protocols use JOIN\_ASK messages in one of two ways:
%(1) a request {\it to} a node to join the secure channel or
%(2) a request {\it by} a node to join the secure channel.
%The Distriubted Protocol uses way (1).
%The Centralized Protocol uses way (2).

CERTIFICATE messages are bandwidth expensive being 4KB in size.
They are most efficiently sent as a Broadcast Announce Message (BAM) and
can be received by all nodes simultaneously.
In the centralized protocol, the MASTER\_CERTIFICATE message utilizes this to distribute the master's certificate.
During the CERTIFICATE exchange, each node sends INTERROGATE to the other node.
A node's response to INTERROGATE is to BAM its CERTIFICATE to the bus.

% how are certificates used in PKI
% -- this is rundundtant on purpose --
Public Key Infrastructure is based on public and private key pairs.
Certificates contain public keys;
they are signed by a trusted third party conferring integrity.
Possession of the associated private key confirms authenticity of node identity.
Each node has a unique public key, private key pair.
CERTIFICATE messages communicate the public key;
this facilitates shared ephemeral key creation.
The session key is then communicated using this ephemeral key.

KEY communicates the session key.
Once all nodes have the KEY message, ACTIVATE\_KEY is transmitted.
In the centralized protocol, 
The master distributes KEY and knows when all nodes have received it.
ACTIVATE\_KEY is then distributed activating the secure channel -- 
nodes begin using the session key contained in KEY.
In the distributed protocol,
nodes activate the secure channel when two ACTIVATE\_KEY messages are observed.
One message is sent by the node sending the initial JOIN\_ASK.
The other is sent by the last node to join the channel.

Nodes using the distributed protocol have logical neighbors.
Neighboring nodes can be seen as a {\it link} in a {\it chain} of nodes.
The RELINK message re-establishes a {\it chain} when a node is removed.
Disconnected neighbors transmit a RELINK message containing the removed neighbor source address.
Subsequently they INTERROGATE each other and complete a CERTIFICATE exchange.

Nodes may unexpectedly restart during key negotiation.
This may result in missing KEY distribution after completion of a CERTIFICATE exchange.
The REQUEST\_KEY message allows the node to request a retransmission of the previous KEY message.
Nodes store the previously transmitted KEY message in memory for this purpose.
This averts renegotiating the key.

%
% freshness synchronization
%

SYNC\_ASK messages request the current freshness value for all out-of-sync counters.
Nodes believing they are out-of-sync send this message.
The requests go to the master node or the last joined node in the centralized and distributed protocols respectively.
SYNC\_ASK contains $HASH(C_0,S_n)\ \|$ $HASH(C_1,S_n)$ $\|$ $..\ \|\ HASH(C_n,S_n)$.
The hash function condenses the freshness counter.
The least significant bits (LSB) of each $HASH$ are included; 
the number of bits per $HASH$ is $128 / |C|$.
This provides the maximum possible number of bits for each freshness value.
Hashes are included in order of ascending source address.
This allows the receiver to associate each hash with its corresponding counter.
The receiver determines which counters are out-of-sync by computing $H(C_i,S_n)$ for each node.
The receiver will send one FRESHNESS message for each non-matching hash;
FRESHNESS messages are not sent for matching hashes; these counters are not out-of-sync. 
Hash collisions are possible.
Another SYNC\_ASK message is sent in this case.
The FRESHNESS message contains a node source address and corresponding freshness counter.

Messages may be transmitted while the FRESHNESS message is queued for sending.
This causes the counter in FRESHNESS to become out-of-sync.
Between SYNC\_ASK and FRESHNESS transmissions, nodes counter the number of messages sent from each source address.
This number is added to the value contained in FRESHNESS.

%
% other situations
%

The OK message is used in response to JOIN\_ASK to confirm a session key is up-to-date.
OK is also used to test node aliveness.
Nodes are expected to send messages at some frequency.
If they fail to do so, then an OK message is sent to them.
They are considered removed unless they respond with OK.

