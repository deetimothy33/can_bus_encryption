%%%
% SOLUTION DESCRIPTION
%%%


% COMMANDS
\newcommand{\protocoltable}{\begin{tabular}{|m{0.3\hsize}|m{0.3\hsize}|m{0.3\hsize}|}}
% surrounds pseudocode for printing
\newcommand{\startpage}{\newpage}
%\renewcommand{\startpage}{}


\startpage
\section{Message Exchanges}
\label{sec-message-exchanges}
% describe
% 	what is the objective
% 	what messages are sent
% 	how sending these messages accomplishes the objective

DATA messages communicate data from sender to receiver(s).
This is accomplished using the frame format described in Section~\ref{sec-frame-format}.
%Messages are a tuple of {\it (message type, data, sender, receiver)}.
%A message exchange is a set of such tuples.
Other message types are transmitted in response to detected bus events.
Resolution of the implicated situations is achieved.

% describe the format of the pseudocode
Message Exchanges have pseudocode descriptions.
{\bf procedure} is an entity implementing {\bf functions}.
An entity is either a generic or master node.
{\bf function} implementations are responses to conditions indicated by the function name.
When a function name is a message, the function body is the response to receiving that message.
%An entity's MAIN function executes once.
ALL CAPS words refer to message types.
Table~\ref{tab-exchange-functions} describes the meaning of functions used throughout pseudocode descriptions.
Functions stop executing if the {\it verifyMAC} function returns false.

%Public, private key pairs for entity $n$ are denoted $e_{n},\ d_n$ respectively.
%Session keys are denoted $S_n$.

This section provides algorithmic descriptions of message exchanges.
These are used in protocols (Section~\ref{sec-protocols}).
Message information is provided in Section~\ref{sec-frame-format}.
Each subsection corresponds to a message exchange.

\begin{table}[!hb]
	\centering
	\begin{tabular}{|m{0.3\hsize}|m{0.6\hsize}|}
		\hline
		{\bf Function} & {\bf Meaning}\\
		\hline
		transmit(M)&Transmit message M.\\
		\hline
		timer(F,S)&Start a timer to execute function F after S seconds.\\
		\hline
		reset(T)&Reset timer T to its initial duration.\\
		\hline
		cancel(T)&Cancel timer T to avoid executing function F.\\
		\hline
		rand()&Generates a cryptographically random number.\\
		\hline
		encrypt(M,K)&Encrypt message M with key K.\\
		\hline
		decrypt(M,K)&Decrypt message M with key K.\\
		\hline
		verifyCERT(C,K)&Verify certificate C using public key K.\\
		\hline
		extractKEY(C)&Extract the public key from certificate C.\\
		\hline
		CURVE25519(D,E)&CURVE25519 public function on private key D and public key E.\\
		\hline
		verifyMAC(M)&Verify the MAC contained in message M.\\
		\hline
		appendMAC(M)&Generate and append MAC to message M.\\
		\hline
		hash(M,K)&Generates a hash of M using key K.\\
		\hline
		freshnessOOS(M)&
		\makecell[l]{
			Performs \hyperref[sec-detect-self-out-of-sync]{freshness test} 
			for M's source address.\\
			Returns true when out-of-sync.
		}\\
		\hline
		lsb(V, N)&Returns the lowest N bits of value V.\\
		\hline
		floor(V)&Returns the floor of value V.\\
		\hline
	\end{tabular}
	\caption{Message Exchange Functions}
	\label{tab-exchange-functions}
\end{table}

%\startpage
\subsection{Key Negotiation}
\label{sec-individual-join-request}
\label{sec-request-node-join}

\subsubsection{Centralized}
\input{tex/protocol/centeralized_join_request.tex}

\startpage
\subsubsection{Distributed}
\input{tex/protocol/distributed_join_request.tex}

\startpage
\subsection{Freshness Synchronization}
\label{sec-exchange-freshness-synchronization}
\input{tex/protocol/freshness_sync.tex}

\startpage
\subsection{Data}
\label{sec-exchange-data}
Normal data message transmissions.
A session key is used to append a MAC to outgoing messages and verify the MAC on incoming messages.
The MAC is a keyed hash using the session key.
It is a hash of concatenated message source address, message data, and freshness counter.

\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	\State long session\_key
	\State long freshness[N] \Comment{N is the number of nodes.}

	\State
	\Function{transmit}{} 
	\State DATA $\gets$ appendMAC(M) \Comment{M is the message.}
	\State transmit(DATA)
	\State freshness[i]$++$ \Comment{Increment this node's freshness counter.}
	\EndFunction
	
	\State
	\Function{data}{}
	\State verifyMAC(DATA)
	\State freshness[i]$++$ \Comment{Increment freshness counter corresponding to DATA's source address.}
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill

\startpage
\subsection{Request Key}
\label{sec-exchange-request-key}
A node may miss KEY distribution.
This occurs in situations such as unexpected restarts.
Nodes may request the current session KEY by transmitting REQUEST\_KEY.
The response to REQUEST\_KEY is retransmission of the KEY message previously sent to the requesting node.
This averts key renegotiation due to offline nodes.
In the centralized protocol, the master is responsible for the response.
In the distributed protocol, a logical neighbor generates the response.

Centralized protocol nodes store the master node's certificate.
Having the master certificate on power up but no session key implies KEY distribution was missed.
Nodes transmit REQUEST\_KEY under this condition.

Distributed protocol nodes have logical neighbors.
When a node restarts after establishing neighbors before receiving KEY,
REQUEST\_KEY is sent.
Logical neighbors send KEY in response.

A timer is started when REQUEST\_KEY is sent; receiving KEY cancels the timer.
Upon timer expiration, it is assumed no response to request key will be given; JOIN\_ASK is sent.
KEY generation and distribution occurs as if a new node is being added.

\subsubsection{Centralized}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	\State long session\_key
	\State long curve\_key
	\State timer request\_timeout
	\State certificate master\_certificate

	\State
	\Function{power\_up}{} \Comment{Node restarted.}
	\If{master\_certificate $\neq$ NULL}
	\State REQUEST\_KEY
	\State request\_timeout $\gets$ timer(transmit(JOIN\_ASK), 1)
	\Else
	\State JOIN\_ASK
	\EndIf
	\EndFunction
	
	\State
	\Function{key}{}
	\State cancel(request\_timeout)
	\State session\_key $\gets$ decrypt(KEY, curve\_key)
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{MASTER}{}
	\State long session\_key
	\State long curve\_key[N] \Comment{N is number of non-master nodes.}

	\State
	\Function{request\_key}{}
	\State KEY $\gets$ encrypt(session\_key, curve\_key[i]) 
		\Comment{i is the index of the requesting node's key}
	\State transmit(KEY)
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill

\startpage
\subsubsection{Distributed}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	\State long session\_key
	\State timer request\_timeout
	\State char neighbor[2] \Comment{Neighbor source address.}
	\State long curve\_key[2]
	
	\State
	\Function{power\_up}{} \Comment{Node restarted.}
	\If{neighbor[0/1] $\neq$ NULL} \Comment{Neighbors previously communicated.}
	\State REQUEST\_KEY
	\State request\_timeout $\gets$ timer(transmit(JOIN\_ASK), 1)
	\Else
	\State JOIN\_ASK
	\EndIf
	\EndFunction

	\State
	\Function{key}{}
	\State cancel(request\_timeout)
	\State session\_key $\gets$ decrypt(KEY, curve\_key[0/1] \Comment{0/1 is 0 or 1.}
	\EndFunction
	
	\State
	\Function{request\_key}{}
	\If{REQUEST\_KEY.sa $=$ neighbor[0/1]} \Comment{.sa is source address.}
	\State KEY $\gets$ encrypt(session\_key, curve\_key[0/1])
	\State transmit(KEY)
	\EndIf
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill

\startpage
\subsection{Relink}
\label{sec-exchange-relink}
In the distributed protocol, neighbors maintain a structure similar to a linked list.
Removed nodes disturb the established logical neighbors.
Upon recognizing a neighbor has been removed, nodes transmit RELINK.
Either one or two RELINK messages are observed.
When one message is observed, the removed node had only one neighbor.
The node sending RELINK generates and propagates KEY.
When two messages are observed, the removed node had two neighbors.
These nodes complete a CERTIFICATE exchange and become neighbors.
A new KEY is generated by the first node to send RELINK.
KEY is propagated both directions along the neighbor structure.
In both cases, end nodes transmit ACTIVATE\_KEY when KEY has finished propagating.
This step occurs in the same way as initial KEY negotiation.

%Upon node removal disconnected SCC neighbors send RELINK. 
%Connection proceds through interrogating one-another.

%Two conditions:\\
%(1) Node in middle of chain is removed.\\
%(2) Node at end of chain is removed.\\

%(1) Two RELINK messages are observed.
%Nodes perform certificate exchange AND renegotiate key.

%(2) One RELINK message is observed.
%Nodes renegotiate key.

%Renegotiated keys propagate to all nodes.

\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	\State long session\_key
	\State long curve\_key[2]

	\State
	\Function{neighbor\_removed}{}
	\State transmit(RELINK)
	\EndFunction
	
	\State
	\Function{relink$\times$2}{} \Comment{Two RELINK messages observed.} 
	\State transmit(INTERROGATE) \Comment{Begins CERTIFICATE exchange with other RELINK transmitter.}
	\EndFunction
	
	\State
	\Function{interrogate}{} 
	\State transmit(CERTIFICATE) 
	\EndFunction
	
	\State
	\Function{certificate}{} 
	\State $\dots$ \Comment{Same as initial KEY negotiation.} 
	\State transmit(KEY) \Comment{Targeted at neighbor[0]}
	\State transmit(KEY) \Comment{Targeted at neighbor[1]}
	\EndFunction
	
	\State 
	\Function{relink$\times$1}{} \Comment{One RELINK message observed.}
	\State session\_key $\gets$ rand()
	\State KEY $\gets$ encrypt(session\_key, curve\_key[i]) 
	\State transmit(KEY) \Comment{Targeted at neighbor; begins key propagation.}
	\EndFunction

	\EndProcedure
\end{algorithmic}
\hrulefill

\startpage
\subsection{Heartbeat}
Nodes use \nameref{sec-detect-heartbeat-membership-detection} to determine if a node has been removed.
Nodes are expected to send messages at some frequency.
A timer is reset each time a message is received from a node.
If no message is received, then an OK message is sent; a timer is started.
The node should respond OK within the timer duration.
If the node fails to respond, then it is considered removed.

\subsubsection{Centralized}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	
	\Function{ok}{}
	\State OK $\gets$ appendMAC(NULL) 
	\State transmit(OK)
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{MASTER}{}
	\State timer aliveness[N] \Comment{N is number of non-master nodes.}

	\State
	\Function{data}{}
	\State verifyMAC(DATA)
	\State reset(aliveness[i]) \Comment{i is based on source address.}
	\EndFunction

	\State
	\Function{heartbeat}{} \Comment{Executed on aliveness timer expiration.}
	\State OK $\gets$ appendMAC(NULL) 
	\State transmit(OK)
	\EndFunction
	
	\State
	\Function{ok}{}
	\State verifyMAC(OK)
	\State reset(aliveness[i]) \Comment{i is based on source address.}
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill

\startpage
\subsubsection{Distributed}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	\State bool test\_aliveness[2] \Comment{True when testing aliveness of a neighbor.}
	\State timer aliveness[2] \Comment{One for each neighbor.}
	
	\State
	\Function{data}{}
	\State verifyMAC(DATA)
	\State reset(aliveness[i]) \Comment{i is based on source address.}
	\EndFunction

	\State
	\Function{heartbeat}{} \Comment{Executed on aliveness timer expiration.}
	\State test\_aliveness[i] $\gets$ true
	\State OK $\gets$ appendMAC(NULL) 
	\State transmit(OK)
	\EndFunction
	
	\State
	\Function{ok}{}
	\State verifyMAC(OK)
	\If{test\_aliveness[i] $=$ true}
	\State test\_aliveness[i] $=$ false
	\State reset(aliveness[i]) \Comment{This node sent OK to check aliveness of a neighbor.}
	\Else	
	\State OK $\gets$ appendMAC(NULL) \Comment{A neighbor sent OK to check aliveness of this node.}
	\State transmit(OK)
	\EndIf
	\EndFunction
	
	\EndProcedure
\end{algorithmic}
\hrulefill


% protocol := { ( scenario, detection mechanism, message exchange ), ...}
%	a set of tuples as described above
\startpage
\section{Protocol Summary}
\label{sec-protocols}
\label{centralized-protocol}
\label{distributed-protocol}
%Protocols exhibiting {\it the three attributes} are developed.
%Message {\it integrity} and {\it the two requirements} for authenticity are verified theoretically;
%Section~\ref{results} provides empirical verification. 

All protocol details have already been described in their entirety.
This section provides a summary and collects the fragmented descriptions into one place.
%Each subsection corresponds to a protocol.
Protocols are seen as a set of tuples of {\it (scenario, detection mechanism, message exchange)}.
Scenario detection and subsequent protocol handling thereof is described.
As in other sections, ALL CAPS words refer to messages.

Figures~\ref{fig-centralized},~\ref{fig-distributed} describe the key negotiation flow in centralized and distributed protocols respectively.
All nodes are connected on a CAN bus.
Arrows indicate the flow of the KEY distribution message.
A shared secret is established between the nodes to distribute key safely.
The square indicates the KEY generation node.
This node also responds to SYNC\_ASK messages.
Nodes with {\bf bold} outlines distribute ACTIVE\_KEY messages to activate a new channel.
Numbering indicates the order nodes join the channel.
In the centralized protocol, the order is arbitrary.
In the distributed protocol, nodes transmitting KEY and ACTIVATE\_KEY depend on this order.

Tables~\ref{tab-centralized},~\ref{tab-distributed} relate
\hyperref[sec-renegotiation-scenario-detection]{Scenarios},
\hyperref[sec-renegotiation-scenario-detection]{Detection Mechanisms}, and
\hyperref[sec-message-exchanges]{Message Exchanges}.
A Scenario is detected using a detection mechanism.
Upon detection, the corresponding message exchange ensues.

% basic vs. improved
%Basic protocols are designed as a trivial case.
%All necessary functionality is provided;
%no performance optimizations are performed.
%Improved protocols encorporate optimizations.

% describe how the protocols are presented
%State machines represent protocols;
%Figure~\ref{fig-state-message-function-variable} enumerates messages, functions, and variables therein.
%Arrows represent branch conditions.
%Circles describe actions taken.
%ALL CAPS denotes message sending.

\subsection{Centralized Protocol}
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\hsize]{centralized.pdf}
	\caption{
		A master node distributes a KEY message to all other nodes.
		KEY is encrypted for each node with a shared secret established by the CERTIFICATE exchange.
	}
	\label{fig-centralized}
\end{figure}

\begin{table}[!htbp]
	\centering
	%\begin{tabular}{|m{3cm}|m{2cm}|m{2cm}|}
	\protocoltable
		\hline
		{\bf Scenario} & {\bf Detection Mechanism} & {\bf Message Exchange}\\
		\hline	
		DATA & -- & \nameref{sec-exchange-data} \\ 
		\hline
		\nameref{sec-network-power-up} & \nameref{sec-detect-network-power-up}
		& \nameref{sec-individual-join-request} \\ 
		%\nameref{sec-key-generation}, 
		%\nameref{sec-exchange-freshness-synchronization}\\
		\hline
		\hyperref[sec-secure-channel-membership-change]{Adding A Node} 
		& Node transmits JOIN\_ASK
		& \nameref{sec-request-node-join} \\ 
		\hline
		\hyperref[sec-secure-channel-membership-change]{Removing A Node} 
		& \nameref{sec-detect-heartbeat-membership-detection}
		& \nameref{sec-request-node-join} \\ 
		\hline
		\nameref{sec-node-freshness-out-of-sync} & \nameref{sec-detect-self-out-of-sync}
		& \nameref{sec-exchange-freshness-synchronization}\\
		\hline
		\hyperref[sec-other-key-renegotiation-scenarios]{100 Operation Hours} 
		& \nameref{sec-detect-operation-hours}
		& \nameref{sec-individual-join-request}\\
		\hline
		\nameref{sec-master-node-replaced} & {\it known by new master} 
		& \nameref{sec-individual-join-request}\\
		%, \nameref{sec-key-generation}, 
		%\nameref{sec-exchange-freshness-synchronization}\\
		\hline
		Missing Key Distribution
		& \nameref{sec-detect-missing-key-distribution}
		& \nameref{sec-exchange-request-key}\\
		\hline
	\end{tabular}
	\caption{Centralized Protocol}
	\label{tab-centralized}
\end{table}

%A simple centralized protocol is described.
%Straightforward but less efficient detection mechanisms and message exchanges are employed.
%This establishs a baseline against which improvement effectiveness is measured.

\subsubsection{Key Distribution}
% initial key distribution
Initial key distribution occurs at power up when no secure channel has been previously established.
Nodes may have disparate power up times;
the master node may not be the first to power up.
Nodes transmit JOIN\_ASK periodically until the master node responds.

The master's response to JOIN\_ASK is INTERROGATE.
Nodes INTERROGATE the master if they have not received MASTER\_CERTIFICATE previously.
The master performs as much other processing as possible before transmitting MASTER\_CERTIFICATE;
this includes receipt of node CERTIFICATE messages.
Certificates are large messages; the number of their transmissions should be minimized.
MASTER\_CERTIFICATE may be received by all online nodes simultaneously.
Delaying its transmission allows time for additional nodes to come online minimizing the number of transmissions.

The master completes one CERTIFICATE exchange with each node.
The resulting shared CURVE25519 secrets encrypt KEY for distribution.
The master then generates a session key, encrypts KEY for each node, and transmits KEY to each node.
After all nodes have received KEY, the master transmits ACTIVATE\_KEY;
nodes begin using the new session key;
freshness counter values are reset to 0.

% removing a node
The master tracks node aliveness using \nameref{sec-detect-heartbeat-membership-detection}.
When a node is removed, the master generates a new KEY.
KEY distribution and activation occur as in initial key distribution.

% adding a node
A node may wish to join and active secure channel.
Upon power up, this node transmits JOIN\_ASK.
The master responds with INTERROGATE.
The node transmits CERTIFICATE and INTERROGATE messages.
The master responds with MASTER\_CERTIFICATE.
The resulting shared CURVE25519 secret encrypts a new session KEY.
The master generates, encrypts, and transmits KEY to all nodes.
When all nodes have received KEY, ACTIVATE\_KEY is transmitted establishing a new secure channel.
%
Notice this sequence of messages is the same as initial key distribution.
Notice also that nodes always respond in the same way when receiving KEY;
they wait for ACTIVATE\_KEY to activate a new secure channel.

\subsubsection{Freshness Synchronization}
Nodes use the method described in Section~\ref{sec-detect-self-out-of-sync} to test their freshness table values.
An incorrect value results in SYNC\_ASK transmission.
The master node responds to SYNC\_ASK messages.
The response is one FRESHNESS message for each out-of-sync freshness value.

FRESHNESS contains the node source address and corresponding freshness counter.
This allows the receiver to update the appropriate freshness table field.
A count of messages sent for each source address 
between SYNC\_ASK and FRESHNESS is added to the freshness counter value.

\subsubsection{Other Scenarios}
% master node replaced
The master node may be replaced.
We assume master nodes know they are master nodes;
their status is given prior to system operation.
Upon power up, the new master node transmits INTERROGATE to each node.
Nodes recognize a new secure channel is being established and INTERROGATE the master node.
CERTIFICATE exchanges and KEY distribution occur as in initial key distribution.

\subsection{Distributed Protocol}
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\hsize]{distributed.pdf}
	\caption{
		The last joining node generates KEY and begins propagating it.
		Each node decrypts and re-encrypts KEY using a shared secret with its neighbor.
		This secret is generated through the CERTIFICATE exchange.
		Two ACTIVATE\_KEY messages initiate the new secure channel.
		Node 0 transmits ACTIVATE\_KEY upon receiving KEY.
		Node 7 transmits ACTIVATE\_KEY in response.
	}
	\label{fig-distributed}
\end{figure}

\begin{table}[!htbp]
	\label{tab-null}
	\centering
	\protocoltable
		\hline
		{\bf Scenario} & {\bf Detection Mechanism} & {\bf Message Exchange}\\
		\hline
		DATA & -- & \nameref{sec-exchange-data} \\ 
		\hline
		\nameref{sec-network-power-up} & \nameref{sec-detect-network-power-up}
		& \nameref{sec-request-node-join} \\ 
		\hline
		\hyperref[sec-secure-channel-membership-change]{Adding A Node} 
		& Node transmits JOIN\_ASK
		& \nameref{sec-request-node-join} \\ 
		\hline
		\hyperref[sec-secure-channel-membership-change]{Removing A Node} 
		& \nameref{sec-detect-heartbeat-membership-detection}
		& \nameref{sec-request-node-join} \\ 
		\hline
		\nameref{sec-node-freshness-out-of-sync} & \nameref{sec-detect-self-out-of-sync}
		& \nameref{sec-exchange-freshness-synchronization}\\
		\hline
		\hyperref[sec-other-key-renegotiation-scenarios]{100 operation hours} 
		& \nameref{sec-detect-operation-hours}
		& \nameref{sec-individual-join-request}\\
		\hline
		\nameref{sec-detect-relink} & \nameref{sec-detect-heartbeat-membership-detection}
		& \nameref{sec-exchange-relink} \\ 
		\hline
		Missing Key Distribution
		& \nameref{sec-detect-missing-key-distribution}
		& \nameref{sec-exchange-request-key}\\
		\hline
	\end{tabular}
	\caption{Distributed Protocol}
	\label{tab-distributed}
\end{table}

%A simple distributed protocol is described.
%Straightforward but less efficient detection mechanisms and message exchanges are employed.
%This establishs a baseline against which improvement effectiveness can be measured.

Secure Channel Chain (SCC) is a logical construct;
it facilitates session key distribution.
Nodes have logical neighbors.
Neighbors create a symmetric secret.
This secures KEY distribution.
Detection mechanisms and freshness synchronization also rely on logical neighbors.
	
\subsubsection{Secure Channel Chain}
\label{sec-secure-channel-chain}
\begin{figure}
	\centering
	\includegraphics[width=0.5\hsize]{bdp_join.pdf}
	\caption{
		Initiator recognizes a session key renegotiation condition;
		it sends the initial JOIN\_ASK.
		Joined Node completes the CERTIFICATE exchange with Initiator;
		they share a CURVE25519 secret.
		Upon joining, the Joined Node
		(1) becomes the previous node's neighbor,
		(2) sends JOIN\_ASK.
		This process continues until all nodes are joined.
		No response to JOIN\_ASK completes the process.
		}
	\label{fig-scc}
\end{figure}

% describe the distributed protocol
A Secure Channel Chain (SCC) is a set of nodes.
Each node has one or two neighbors;
similar to a linked list.
Nodes with one neighbor are the ends of the chain.
Nodes with two neighbors are links in the chain.
Neighbors share a CURVE25519 secret.

Figure~\ref{fig-scc} describes Secure Channel Chain (SCC) construction.
An initiator node realizes session key renegotiation is necessary;
this node sends the initial JOIN\_ASK message.
The message is a request for the receiver to join the SCC.

Nodes not yet in SCC respond to JOIN\_ASK with INTERROGATE.
The node which transmits the first INTERROGATE message to win arbitration will become the joining node.
The joining node and the JOIN\_ASK sender exchange CERTIFICATE messages;
they establish a shared CURVE25519 secret.
The joining node will then transmit a JOIN\_ASK message.
Each node joined requests that a single other node be joined.
The process ends when there is no response to a JOIN\_ASK message.
This concludes SCC establishment.

\subsubsection{Key Distribution}
% initial key distribtion
SCC facilitates key distribution.
The last joining node generates and transmits the session KEY to its neighbor.
SCC neighbors have a shared CURVE25519 secret generated by the CERTIFICATE exchange during SCC construction;
it protects KEY.
Nodes receiving KEY decrypt it using this secret;
they learn the session key.
Nodes then re-encrypt the session key using the CURVE25519 secret shared with their other neighbor;
it is then transmitted.

KEY continues propagating along the chain until it reaches the node that sent the initial JOIN\_ASK.
Upon decrypting KEY, this node transmits ACTIVATE\_KEY.
Upon observing ACTIVATE\_KEY, the last joining node transmits ACTIVATE\_KEY.
Upon observing two ACTIVATE\_KEY messages, all nodes begin using the session key;
freshness counter values are reset to 0.
Nodes at the SCC ends are the only nodes with one neighbor.
This provides a straightforward condition for sending ACTIVATE\_KEY.
{\it If there is one neighbor and KEY is received, then transmit ACTIVATE\_KEY.}

% relinking -- removed nodes
A removed node may be detected.
This will create a missing link in the SCC.
Nodes track their neighbors' aliveness using \nameref{sec-detect-heartbeat-membership-detection}.
There are two conditions when a node becomes aware of a disconnected neighbor:
(1) the neighbor was the end of the chain or
(2) the neighbor was a link in the chain.
Upon becoming aware of a disconnected neighbor, a node transmits RELINK.
In condition (1), one RELINK messages will be observed.
The node detecting the removed node will generate and propagate KEY.
Channel activation occurs in the same way as in initial key distribution.
In condition (2), two RELINK messages will be observed.
Nodes sending RELINK will INTERROGATE each other and complete a CERTIFICATE exchange.
The result is the creation of a new link in the chain.
The first node to transmit RELINK will generate KEY and begin propagating it both directions.
Nodes at the SCC ends generate ACTIVATE\_KEY in response to receiving the KEY message.
When two ACTIVATE\_KEY messages are observed, nodes begin using the new session key.

% adding a node -- key distribtion in an active network
A node may want to join an active network where a session key has already been established.
This node will join the end of the existing SCC.
When the new node powers up, it transmits INTERROGATE.
The last joining node currently in the SCC responds with INTERROGATE.
These nodes complete a CERTIFICATE exchange.
The new node generates and transmits KEY.
Activation of this KEY occurs in the same way as initial key distribution.

% why two activate_key messages
Two ACTIVATE\_KEY messages are used to create a universal condition to begin using a new session key.
Initial key negotiation, removed node relinking, and adding a node to an activate network terminate with the transmission of two ACTIVATE\_KEY messages.

\subsubsection{Freshness Synchronization}
Nodes use the method described in Section~\ref{sec-detect-self-out-of-sync} to test their freshness table values.
An incorrect value results in SYNC\_ASK transmission.
The last joining SCC node assumes the responsibility for responding SYNC\_ASK messages.
The response is one FRESHNESS message for each out-of-sync freshness value.
FRESHNESS content is the same as in the centralized protocol.

\subsubsection{Other Scenarios}
% 100 operation hours
The last joining SCC node becomes responsible for detecting the
\hyperref[sec-other-key-renegotiation-scenarios]{100 operation hours} scenario.
Upon detection, this node generates and transmits a new session key.
KEY propagation and activation occur in the same way as initial key distribution.

% missing key distribution
When a node misses KEY distribution it will transmit REQUEST\_KEY.
This message is targeted at one of its neighbors.
The response is a retransmission of the previous KEY message.
