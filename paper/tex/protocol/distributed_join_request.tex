A logical structure called \hyperref[sec-secure-channel-chain]{Secure Channel Chain (SCC)} is established.
It is a set of logical neighbors similar to a linked list.
Each node has two neighbors.
These neighbors share a CURVE25519 secret.
This is used to propagate the session key from node to node.
Each node decrypts KEY to acquire the session key.
The session key is then re-encrypted using the secret shared with the other logical neighbor.
Transmitting KEY in this way propagates KEY through the entire set of nodes.
The last node to join the SCC begins KEY propagation.
Nodes at each end of the SCC transmit ACTIVATE\_KEY to begin using the new session key.

At power up, nodes start a timer for a random short amount of time.
At timer expiration, JOIN\_ASK is transmitted.
Other nodes will cancel their timers when they observe JOIN\_ASK;
this ensures there is one initial starting node.
Nodes respond to JOIN\_ASK with INTERROGATE.
The first INTERROGATE placed on the bus will become a neighbor to the node transmitting JOIN\_ASK.
These nodes perform a CERTIFICATE exchange.
The last joining SCC node then transmits JOIN\_ASK.
This is a request for other nodes to join the secure channel.
It will respond to the first INTERROGATE to be placed on the bus.

Upon transmitting JOIN\_ASK, nodes begin a timer.
If no INTERROGATE is transmitted before timer expiration,
then all nodes are hypothesized to be joined.
The last node to join the SCC generates and begins propagating KEY.

Adding a secure channel node requires key renegotiation.
A node wishing to join an active channel initially transmits JOIN\_ASK at power up.
However, no response is received because the channel is already established.
This node has no neighbors; it hypothesizes there is an active secure channel.
It will then broadcast INTERROGATE.
The last joining SCC node completes a CERTIFICATE exchange with this node.
The new node becomes the last joining SCC node;
it generates and begins propagating KEY.

\startpage
\vspace*{-1.8cm}
\hrulefill
\begin{algorithmic}[1]
	\Procedure{NODE}{}
	\State long TTP\_key \Comment{Public key of Trusted Third Party -- the signer of certificates.}
	\State long private\_key
	\State long session\_key
	\State long next\_session\_key
	\State timer initiate\_timer \Comment{Initiate SCC construction at expiration.}
	\State timer last\_node\_timer \Comment{Nodes know they are the last joining node at expiration.}
	\State bool last\_node \Comment{True for the last joining node.}
	\State bool accept\_interrogate \Comment{True when accepting first INTERROGATE on the bus.}
	\State bool accept\_certificate \Comment{Nodes only accept certificates after INTERROGATE.}
	\State bool joining\_node \Comment{True when in the process of joining.}
	\State int activate\_key\_count \Comment{Two ACTIVATE\_KEY messages activate the secure channel.}
	\State long freshness[N] \Comment{N is the number of nodes.}
	\State long curve\_key[2] 
	\State char neighbor[2] \Comment{Neighbor source address.}
	\State char source\_address \Comment{Source address of this node.}

	\State 
	\Function{powerup}{}
	\State initiate\_timer $\gets$ timer(INITIATE\_SCC, $(rand()\% 3)+1$) 
		\Comment{Random number between 1 and 3.}
	\EndFunction
	
	\State 
	\Function{initiate\_SCC}{}
	%\State JOIN\_ASK $\gets$ rand() $\|$ hash(rand(), session\_key)
	\State transmit(JOIN\_ASK) \Comment{Initiates SCC construction.}
	\State last\_node\_timer $\gets$ timer(ACTIVATE, 5) \Comment{Begin propagating key after 5 seconds.}
	\State accept\_interrogate $\gets$ true \Comment{Accept first INTERROGATE on bus.}
	\EndFunction
	
	\State 
	\Function{join\_ask}{}
	\If{neighbor[0,1] $=$ NULL} \Comment{Stop accepting JOIN\_ASK once there is a neighbor.}
	\State joining\_node $gets$ true
	\State cancel(initiate\_timer)
	\State transmit(INTERROGATE)
	\State accept\_certificate $\gets$ true
	\EndIf
	\EndFunction

	\State
	\Function{interrogate}{}
	\If{accept\_interrogate}
	\State accept\_interrogate $\gets$ false \Comment{Stop accepting INTERROGATE messages.}
	\State cancel(last\_node\_timer)
	\State transmit(INTERROGATE) \Comment{Interrogate joining node.}
	\State transmit(CERTIFICATE) \Comment{Present certificate.}
	\State accept\_certificate $\gets$ true
	\EndIf
	\If{joining\_node}
	\State transmit(CERTIFICATE) \Comment{Present certificate.}
	\EndIf
	\If{last\_node} \Comment{Last joining node responds to request to join an active secure channel.}
	\State last\_node $\gets$ false
	\State transmit(INTERROGATE) \Comment{Interrogate joining node.}
	\State transmit(CERTIFICATE) \Comment{Present certificate.}
	\State accept\_certificate $\gets$ true
	\EndIf
	\EndFunction
	
	\State 
	\Function{certificate}{}
	\If{CERTIFICATE.dest $=$ source\_address $\&$ accept\_certificate}
	\If{verifyCERT(CERTIFICATE, TTP\_key)} 
	\State accept\_certificate $\gets$ false
	\State neighbor[0/1] $\gets$ CERTIFICATE.sa \Comment{.sa extracts the source address. 0/1 is 0 or 1.}
	\State node\_public\_key $\gets$ extractKEY(CERTIFICATE)
	\State curve\_key[0/1] $\gets$ CURVE25519(private\_key, node\_public\_key) 
	\If{joining\_node}
	\State joining\_node=false;
	\State transmit(JOIN\_ASK);
	\State last\_node\_timer $\gets$ timer(ACTIVATE, 5) 
	\State accept\_interrogate $\gets$ true 
	\EndIf
	\EndIf 
	\EndIf
	\EndFunction
	
	\State 
	\Function{activate}{}
	\If{neighbor[0,1] $=$ NULL} \Comment{This node has no neighbors.}
	\State transmit(INTERROGATE) \Comment{Try to join an active secure channel.}
	\State accept\_certificate $\gets$ true
	\Else
	\State last\_node $\gets$ true \Comment{This is the last joining node.}
	\State next\_session\_key $\gets$ rand()
	\ForAll{i $\in$ [0,1]} \Comment{For each neighbor; there may be only 1.}
	\State KEY $\gets$ encrypt(next\_session\_key, curve\_key[i]) 
		\Comment{Encrypt KEY using CURVE25519 secret.}
	\State transmit(KEY) \Comment{Distribute KEY to each Neighbor.}
	\EndFor
	\EndIf
	\EndFunction
	
	\State 
	\Function{key}{}
	\If{KEY.dest $=$ source\_address} \Comment{.dest extracts destination address.}
	\State next\_session\_key $\gets$ decrypt(KEY, curve\_key[0/1]) \Comment{Receive KEY from neighbor.}
	\If{neighbor[1/0] $\neq$ NULL} \Comment{If this node has two neighbors.}
	\State KEY $\gets$ encrypt(next\_session\_key, curve\_key[1/0]) \Comment{Encrypt KEY for other neighbor.}
	\State transmit(KEY)
	\Else \Comment{This node only has one neighbor.}
	\State transmit(ACTIVATE\_KEY)
	\State activate\_key\_count++
	\EndIf
	\EndIf
	\EndFunction
	
	\State 
	\Function{activate\_key}{}
	\State activate\_key\_count++
	\If{activate\_key\_count $=$ 1 $\&\&$ last\_node}
	\State transmit(ACTIVATE\_KEY) \Comment{Last joining node transmits second ACTIVATE\_KEY.}
	\State activate\_key\_count++
	\EndIf	
	\If{activate\_key\_count $=$ 2}
	\State session\_key $\gets$ next\_session\_key 
	\ForAll{i $\in$ [0..N-1]} \Comment{For all i in range 0 to N-1.}
	\State freshness[i] $\gets$ 0 \Comment{Reset freshness.}
	\EndFor
	\State activate\_key\_count $\gets$ 0
	\EndIf
	\EndFunction

	\State
	\Function{request\_key}{}
	\If{REQUEST\_KEY.sa $=$ neighbor[0/1]} \Comment{.sa extracts the source address.}
	\State KEY $\gets$ encrypt(next\_session\_key, curve\_key[0/1]) 
	\State transmit(KEY)
	\EndIf
	\EndFunction

	\EndProcedure
\end{algorithmic}
\hrulefill

