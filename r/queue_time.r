#! /bin/Rscript

# REFERENCE
# https://www.tutorialspoint.com/r/r_normal_distribution.htm

# TIME := amount

# DEFINE VARIABLES
# distribution
# am i not using queue_depth as a proxy for time?
#	they should be the same quantity
MEAN<-3
SD<-1
queue_depth<-seq(0,6,by=.1)
y<-dnorm(queue_depth,mean=MEAN,sd=SD)
# number of nodes
N<-10
# number of message types
M<-10

# COMPUTE TOTAL MESSAGE FREQUNECY WITH EACH SCHEME
#TODO it seems like there should be a different frequency for each message type
#	how to compute the total frequency of each message?
F_ovf<-1
F_pwf<-2
F_pmf<-3
F_pmpwf<-4

# COMPUTE PROBABILITY

# lines are placed at
# PERIOD * QUEUE_DEPTH
T_ovf<-1/F_ovf*MEAN
T_pwf<-1/F_pwf*MEAN
T_pmf<-1/F_pmf*MEAN
T_pmpwf<-1/F_pmpwf*MEAN

# PRINT DISTRIBUTION
png(file="queue_time.png")
# distribution
plot(queue_depth, y)
# periods of things
#abline(v=T_ovf)
#text(T_ovf,0.2,"ovf")
#abline(v=T_pwf)
#text(T_pwf,0.3,"pwf")
#abline(v=T_pmf)
#text(T_pmf,0.4,"pmf")
#abline(v=T_pmpwf)
#text(T_pmpwf,0.2,"pmpwf")
dev.off()
