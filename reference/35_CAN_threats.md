#abs
4 attack scenarios are presented (S1-S4)
discusses countermeasures

#intro
REF[2] forged traffic into navigation systems using wireless protocols RDS, TMC
REF[3] car 2 car (C2C), car 2 infrastructure (C2I)
REF[6] CERT taxonomy

#state-of-the-art
keyless entry is challenge-response mechanism
	1. car generates random challenge
	2. key generates response
	3. upon receiving the correct response, car doors open
**sensor, actuator**

#3 -- 4 threats


#summary and outlook
existing solutions are presented

