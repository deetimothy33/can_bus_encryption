#! /bin/ruby

SRC_DIR="../simulator/code/src/"

# Template for each src file
def src_template(file_name)
	s=""

	# file name as section heading
	s+='\newpage'
	s+="\\section\{#{file_name}\}"
	s+="\n"

	s+="\\lstinputlisting\{#{SRC_DIR}#{file_name}\}"

	s
end

# Generate the replacement for the replaceable section of the template
# Contains code for each simulator file
def replace
	s=""

	#s+="\\newpage"
	#s+="\n"
	s+="\\tableofcontents"
	s+="\n"

	Dir.foreach(SRC_DIR) do |srcfile|
		next if srcfile == '.' or srcfile == '..' or srcfile[0] == '.'
		s += src_template(srcfile);
	end

	s
end

def main
	File.open("paper.tex","w") do |outfile|
		File.open("template.tex","r") do |infile|
			infile.each_line do |line|
				# find the replace line, otherwise output the template
				if(line.eql? "%REPLACE\n")
					outfile.puts replace()
				else
					outfile.puts line
				end
			end
		end
	end
end

main()
