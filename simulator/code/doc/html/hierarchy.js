var hierarchy =
[
    [ "BaseCrypto", "class_base_crypto.html", [
      [ "RealCrypto", "class_real_crypto.html", null ],
      [ "SimulatedCrypto", "class_simulated_crypto.html", null ]
    ] ],
    [ "CertificatePacketData", "union_certificate_packet_data.html", null ],
    [ "ECU", "class_e_c_u.html", [
      [ "Node", "class_node.html", null ]
    ] ],
    [ "Gateway", "class_gateway.html", null ],
    [ "high_resolution_clock", null, [
      [ "Clock", "class_clock.html", null ]
    ] ],
    [ "iostream", null, [
      [ "Bus", "class_bus.html", null ],
      [ "CANSocket", "class_c_a_n_socket.html", null ]
    ] ],
    [ "Metric", "struct_metric.html", null ],
    [ "Packet", "class_packet.html", [
      [ "BasePacket", "class_base_packet.html", [
        [ "ActivateKeyPacket", "class_activate_key_packet.html", null ],
        [ "CertificatePacket", "class_certificate_packet.html", null ],
        [ "FreshnessPacket", "class_freshness_packet.html", null ],
        [ "InterrogatePacket", "class_interrogate_packet.html", null ],
        [ "JoinAskPacket", "class_join_ask_packet.html", null ],
        [ "KeyPacket", "class_key_packet.html", null ],
        [ "OKPacket", "class_o_k_packet.html", null ],
        [ "SecurePacket", "class_secure_packet.html", null ],
        [ "SyncAskPacket", "class_sync_ask_packet.html", null ]
      ] ]
    ] ],
    [ "Protocol", "class_protocol.html", [
      [ "BaseProtocol", "class_base_protocol.html", [
        [ "CenteralizedProtocol", "class_centeralized_protocol.html", [
          [ "BasicCenteralizedProtocolMaster", "class_basic_centeralized_protocol_master.html", null ],
          [ "BasicCenteralizedProtocolNode", "class_basic_centeralized_protocol_node.html", null ]
        ] ],
        [ "DistributedProtocol", "class_distributed_protocol.html", [
          [ "BasicDistributedProtocolNode", "class_basic_distributed_protocol_node.html", null ]
        ] ]
      ] ]
    ] ],
    [ "SecurePacketData", "union_secure_packet_data.html", null ],
    [ "Simulation", "class_simulation.html", [
      [ "TestSimulation", "class_test_simulation.html", null ],
      [ "XMLSimulation", "class_x_m_l_simulation.html", null ]
    ] ]
];