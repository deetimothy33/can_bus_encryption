\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Activate\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Key\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Base\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Crypto Class Reference}{5}{section.3.2}
\contentsline {section}{\numberline {3.3}Base\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{6}{section.3.3}
\contentsline {section}{\numberline {3.4}Base\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Protocol Class Reference}{7}{section.3.4}
\contentsline {section}{\numberline {3.5}Basic\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Centeralized\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Protocol\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Master Class Reference}{8}{section.3.5}
\contentsline {section}{\numberline {3.6}Basic\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Centeralized\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Protocol\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Node Class Reference}{8}{section.3.6}
\contentsline {section}{\numberline {3.7}Basic\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Distributed\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Protocol\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Node Class Reference}{9}{section.3.7}
\contentsline {section}{\numberline {3.8}Bus Class Reference}{9}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Detailed Description}{10}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Member Function Documentation}{10}{subsection.3.8.2}
\contentsline {subsubsection}{\numberline {3.8.2.1}operator$<$$<$()}{10}{subsubsection.3.8.2.1}
\contentsline {section}{\numberline {3.9}C\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Socket Class Reference}{11}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Detailed Description}{11}{subsection.3.9.1}
\contentsline {section}{\numberline {3.10}Centeralized\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Protocol Class Reference}{11}{section.3.10}
\contentsline {section}{\numberline {3.11}Certificate\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{12}{section.3.11}
\contentsline {section}{\numberline {3.12}Certificate\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Data Union Reference}{13}{section.3.12}
\contentsline {section}{\numberline {3.13}Clock Class Reference}{13}{section.3.13}
\contentsline {subsection}{\numberline {3.13.1}Detailed Description}{13}{subsection.3.13.1}
\contentsline {section}{\numberline {3.14}Distributed\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Protocol Class Reference}{14}{section.3.14}
\contentsline {section}{\numberline {3.15}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}CU Class Reference}{14}{section.3.15}
\contentsline {section}{\numberline {3.16}Freshness\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{15}{section.3.16}
\contentsline {section}{\numberline {3.17}Gateway Class Reference}{15}{section.3.17}
\contentsline {subsection}{\numberline {3.17.1}Detailed Description}{15}{subsection.3.17.1}
\contentsline {section}{\numberline {3.18}Interrogate\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{16}{section.3.18}
\contentsline {section}{\numberline {3.19}Join\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Ask\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{16}{section.3.19}
\contentsline {section}{\numberline {3.20}Key\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{16}{section.3.20}
\contentsline {section}{\numberline {3.21}Metric Struct Reference}{17}{section.3.21}
\contentsline {section}{\numberline {3.22}Node Class Reference}{17}{section.3.22}
\contentsline {subsection}{\numberline {3.22.1}Detailed Description}{18}{subsection.3.22.1}
\contentsline {section}{\numberline {3.23}O\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}K\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{18}{section.3.23}
\contentsline {section}{\numberline {3.24}Packet Class Reference}{18}{section.3.24}
\contentsline {subsection}{\numberline {3.24.1}Detailed Description}{19}{subsection.3.24.1}
\contentsline {section}{\numberline {3.25}Protocol Class Reference}{20}{section.3.25}
\contentsline {subsection}{\numberline {3.25.1}Detailed Description}{21}{subsection.3.25.1}
\contentsline {section}{\numberline {3.26}Real\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Crypto Class Reference}{21}{section.3.26}
\contentsline {section}{\numberline {3.27}Secure\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{21}{section.3.27}
\contentsline {section}{\numberline {3.28}Secure\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Data Union Reference}{22}{section.3.28}
\contentsline {section}{\numberline {3.29}Simulated\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Crypto Class Reference}{22}{section.3.29}
\contentsline {section}{\numberline {3.30}Simulation Class Reference}{23}{section.3.30}
\contentsline {subsection}{\numberline {3.30.1}Detailed Description}{23}{subsection.3.30.1}
\contentsline {section}{\numberline {3.31}Sync\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Ask\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Packet Class Reference}{24}{section.3.31}
\contentsline {section}{\numberline {3.32}Test\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Simulation Class Reference}{24}{section.3.32}
\contentsline {section}{\numberline {3.33}X\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}L\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Simulation Class Reference}{25}{section.3.33}
