#! /bin/bash

# REFERENCE
# https://en.wikipedia.org/wiki/SocketCAN

# USAGE
# ./simulator.bash [simulation] [generate traffic]
# 	[simulation] -- integer selecting simulation to run
# 	[generate traffic] -- generates grafic if any argument is provided


# test input
if [ "$#" -lt 1 ]; then
	#echo "USAGE: $0 [bus load percent]"
	# print help message
	./main.x
	exit
fi

# set up can and vcan kernel modules
# lib/ICSim/setup_vcan.sh
sudo modprobe can
sudo modprobe vcan
sudo ip link add dev vcan0 type vcan
sudo ip link set up vcan0

# start can-utils logging utility
lib/can-utils/candump vcan0 > log/simulator.txt &
CANDUMP_PID=$!

# generate bus random test bus loading
#if [ "$#" -ne 0 ]; then
if [ "$#" -gt 1 ]; then
	echo "generating traffic"
	# -f 		canFD packets
	# -g <ms>	gap between packets
	# -I		packet ID
	# -L		packet data length
	lib/can-utils/cangen vcan0 -f -g 1 -I 7FF -L 16 &
	CANGEN_PID=$!
fi

# execute the simulator
./main.x $1

# stop can-utils logging utility
if [ "$#" -gt 1 ]; then
	kill $CANGEN_PID
fi

kill $CANDUMP_PID
