#include "CenteralizedProtocol.h"

void CenteralizedProtocol::main(Bus* bus){
	BaseProtocol::main(bus);
}

void CenteralizedProtocol::process(Packet *packet){
	BaseProtocol::process(packet);
}

void CenteralizedProtocol::powerup(){
	BaseProtocol::powerup();	
}


// NODE

void BasicCenteralizedProtocolNode::powerup(){
	CenteralizedProtocol::powerup();

	// this method is called once
	Packet *p;

	// did I miss key distribution?
	if(master_certificate!=0 && session_key==0){
		p=new RequestKeyPacket();
		send_queue.push(p);
	}else{
		// sending JOIN_ASK initiates the process of joining the network
		Packet *p;
		p=new JoinAskPacket();
		send_queue.push(p);
	}
}

void BasicCenteralizedProtocolNode::main(Bus* bus){
	CenteralizedProtocol::main(bus);
}

void BasicCenteralizedProtocolNode::process(Packet *packet){
	CenteralizedProtocol::process(packet);
	
	// 2. determine if the packet requires any behavior from this ECU
	// 3. add any packet ECU needs to send to the send_queue
	Packet *p;
	
	if(packet->PDU_format()==MASTER_CERTIFICATE){
		if(packet->verify(TTP_key)){
			//TODO extract master certificate from data
			//TODO extract node public key from data
			master_certificate=packet->data[1];
			Key node_public_key=packet->data[0];
			curve_key=Crypto::curve25519_public_function(private_key, node_public_key);
		}
		cout << "master_certificate" << endl;
	}
	if(packet->PDU_format()==INTERROGATE){
		p=new CertificatePacket(); 
		send_queue.push(p);
		if(master_certificate==0 && packet->PDU_specific()==source_address){
			p=new InterrogatePacket(); 
			send_queue.push(p);
		}
		cout << "interrogate" << endl;
	}
	if(packet->PDU_format()==KEY){
		// if the packet has destination address equal to my source address
		if(packet->PDU_specific()==source_address){
			packet->decrypt(curve_key);
			//TODO extract data from packet
			next_session_key=packet->data[0];
			
			cout << "key" << endl;
		}
	}
	if(packet->PDU_format()==ACTIVATE_KEY){
		cout << "activate key" << endl;
		
		session_key=next_session_key;
		for(auto const& x : freshness_map){
			// reset freshness counters
			freshness_map[x.first]=0;
		}
	}
}


// MASTER

void BasicCenteralizedProtocolMaster::powerup(){
	CenteralizedProtocol::powerup();

	// activate the secure channel when the timer goes off
	establish_timer=new Timer(std::chrono::milliseconds(3000));
}

void BasicCenteralizedProtocolMaster::main(Bus* bus){
	CenteralizedProtocol::main(bus);

	Packet *p;

	// act on any expired timers
	if(establish_timer && establish_timer->expired){
		// function ACTIVATE
		establish_timer->cancel();

		// begin KEY propagation
		next_session_key=rand();
		// propagate to all other nodes
		// x.first is source address of nodes
		for(auto const& x : curve_key_map){
			p=new KeyPacket();
			p->set_PDU_specific(x.first);
			p->set_data(next_session_key);
			p->encrypt(curve_key_map[x.first]);
			send_queue.push(p);
		}
		
		p=new ActivateKeyPacket();
		send_queue.push(p);
		
		session_key=next_session_key;
		for(auto const& x : freshness_map){
			// reset freshness counters
			freshness_map[x.first]=0;
		}
		cout << "BEGIN KEY PROPAGATION" << endl;
	}
}

void BasicCenteralizedProtocolMaster::process(Packet *packet){
	CenteralizedProtocol::process(packet);

	Packet *p;
	
	// 2. determine if the packet requires any behavior from this ECU
	// 3. add any packet ECU needs to send to the send_queue
	if(packet->PDU_format()==JOIN_ASK){
		//TODO check join_ask key hash
		//	does it match what it should?
		bool key_hash_match=false;
		if(key_hash_match) p=new OKPacket();
		else{
			p=new InterrogatePacket();
			p->set_PDU_specific(packet->source_address());
			establish_timer->reset();
		}
		send_queue.push(p);
		cout << "join_ask" << endl;
	}
	if(packet->PDU_format()==CERTIFICATE){
		//TODO extract key from certificate
		Key node_public_key=packet->data[0];
		curve_key_map[packet->source_address()]=Crypto::curve25519_public_function(private_key, node_public_key);
		cout << "certificate" << endl;
	}
	// upon interrogation, present certificate
	if(packet->PDU_format()==INTERROGATE){
		//TODO delay sending master certificate
		p=new MasterCertificatePacket(); 
		send_queue.push(p);
		cout << "interrogate" << endl;
	}
	if(packet->PDU_format()==REQUEST_KEY){
		//TODO
		// retransmit the key
		cout << "request_key" << endl;
	}
	if(packet->PDU_format()==SYNC_ASK){
		//TODO make a method in protocol to do the sync check
		process_sync_ask(packet);
		cout << "sync_ask" << endl;
	}
}


// SYNC ASK NODE

void BasicCenteralizedProtocolSyncAskNode::powerup(){
	BasicCenteralizedProtocolNode::powerup();

	// suppose the node falls out of sync after 5 seconds
	sync_ask_timer=new Timer(std::chrono::milliseconds(5000));
}

void BasicCenteralizedProtocolSyncAskNode::main(Bus* bus){
	BasicCenteralizedProtocolNode::main(bus);
	
	// randomize the freshness_map
	for(auto const& x : freshness_map){
		freshness_map[x.first]=rand();
	}

	if(sync_ask_timer && sync_ask_timer->expired){
		sync_ask_timer->cancel();
		send_sync_ask();
	}
}
