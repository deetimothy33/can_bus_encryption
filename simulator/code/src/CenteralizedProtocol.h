#ifndef CENTRALIZED_PROTOCOL_H
#define CENTRALIZED_PROTOCOL_H

#include "Protocol.h"
#include "Timer.h"

// common for all centeralized protocols
class CenteralizedProtocol : public BaseProtocol{
	public:

	virtual void main(Bus* bus);
	virtual void process(Packet *packet);
	virtual void powerup();
};

class BasicCenteralizedProtocolNode : public CenteralizedProtocol{
	public:
	uint64_t curve_key;
	//CertificatePacketData master_certificate=0;
	uint64_t master_certificate=0;

	virtual void main(Bus* bus);
	virtual void process(Packet *packet);
	virtual void powerup();
};

class BasicCenteralizedProtocolMaster : public CenteralizedProtocol{
	public:
	Timer *establish_timer=0;
	map<uint8_t,uint64_t> curve_key_map;

	virtual void main(Bus* bus);
	virtual void process(Packet *packet);
	virtual void powerup();
};


class BasicCenteralizedProtocolSyncAskNode : public BasicCenteralizedProtocolNode{
	public:
	Timer *sync_ask_timer;

	virtual void main(Bus* bus);
	virtual void powerup();
};

#endif
