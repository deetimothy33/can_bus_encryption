#include "Crypto.h"

//TODO .... obviously
	
//TODO test this function
// create a mask from a to b
// (1's from [a,b] => inclusive)
uint128_t BaseCrypto::mask(unsigned int a, unsigned int b){
	uint128_t mask = 0;
	for(unsigned int i=a; i<=b; i++){
		mask |= (1 << i);
	}

	return mask;
}

Key BaseCrypto::curve25519_public_function(Key private_key, Key node_public_key){
	//TODO
	return rand();
}

uint8_t BaseCrypto::hash(uint64_t value){
	//TODO a silly hash function
	srand(value);
	return rand();
}


uint128_t SimulatedCrypto::aes_cmac(Key key, uint128_t data){ return 0; }
uint32_t SimulatedCrypto::ed25519(Key key, uint128_t data){ return 0; }
uint128_t SimulatedCrypto::curve25519_encrypt(Key key, uint128_t data){ return 0; }
uint128_t SimulatedCrypto::curve25519_decrypt(Key key, uint128_t data){ return 0; }

uint128_t RealCrypto::aes_cmac(Key key, uint128_t data){ return 0; }
uint32_t RealCrypto::ed25519(Key key, uint128_t data){ return 0; }
uint128_t RealCrypto::curve25519_encrypt(Key key, uint128_t data){ return 0; }
uint128_t RealCrypto::curve25519_decrypt(Key key, uint128_t data){ return 0; }
