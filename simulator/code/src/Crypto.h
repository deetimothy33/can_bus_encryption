#ifndef CRYPTO_H
#define CRYPTO_H

#include <cstdint>
#include <stdlib.h>

using namespace std;

/**
 * provides static methods for all cryptography operations
 * the purpose in doing this is:
 * 	1 to allow easy switching between simulated and real cryptography
 * 	2 make the rest of the code less messy
 * 	3 centralize all cryptographic code in one place
 */

typedef unsigned __int128 Key;
typedef unsigned __int128 uint128_t;

// implements things needed by both Simulated and Real crypto
class BaseCrypto{
	public:
	static uint128_t mask(unsigned int a, unsigned int b);
	static Key curve25519_public_function(Key private_key, Key node_public_key);
	//TODO implement a real hash function
	static uint8_t hash(uint64_t value);
};

class SimulatedCrypto : public BaseCrypto{
	public:
	static uint128_t aes_cmac(Key key, uint128_t data);
	static uint32_t ed25519(Key key, uint128_t data);
	static uint128_t curve25519_encrypt(Key key, uint128_t data);
	static uint128_t curve25519_decrypt(Key key, uint128_t data);
};

class RealCrypto : public BaseCrypto{
	public:
	static uint128_t aes_cmac(Key key, uint128_t data);
	static uint32_t ed25519(Key key, uint128_t data);
	static uint128_t curve25519_encrypt(Key key, uint128_t data);
	static uint128_t curve25519_decrypt(Key key, uint128_t data);
};

// NOTE: change this to switch between simulated and real crypto
// the rest of the code uses Crypto::[function]() to do cryptography
typedef SimulatedCrypto Crypto;

#endif
