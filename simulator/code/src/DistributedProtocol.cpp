#include "DistributedProtocol.h"

void DistributedProtocol::main(Bus* bus){
	BaseProtocol::main(bus);
}

void DistributedProtocol::process(Packet *packet){
	BaseProtocol::process(packet);
}

void DistributedProtocol::powerup(){
	BaseProtocol::powerup();
}


void BasicDistributedProtocolNode::powerup(){
	DistributedProtocol::powerup();
	
	initiate_timer=new Timer(std::chrono::milliseconds((rand()%3001)+1000));	
}

void BasicDistributedProtocolNode::main(Bus* bus){
	DistributedProtocol::main(bus);
		
	Packet *p;

	// act on any expired timers
	if(initiate_timer && initiate_timer->expired){
		initiate_timer->cancel();
		
		// send JOIN_ASK packet
		p=new JoinAskPacket();
		send_queue.push(p);
		// start the last_node_timer incase there is no response
		last_node_timer=new Timer(std::chrono::milliseconds(1000));
		accept_interrogate=true;
		
		cout << "INITIATE" << endl;
	}

	if(last_node_timer && last_node_timer->expired){
		last_node_timer->cancel();

		// function ACTIVATE
		if(neighbor_v.size() == 0){
			// there are no neighbors
			// a secure channel of 1 makes no sense
			// try to join an active secure channel
			p=new InterrogatePacket();
			send_queue.push(p);
			accept_certificate=true;
		}else{
			// begin KEY propagation
			last_node=true;
			next_session_key=rand();
			// propagate to each neighbor
			for(auto const& x : neighbor_v){
				p=new KeyPacket();
				p->set_PDU_specific(x);
				p->set_data(next_session_key);
				p->encrypt(curve_key_map[x]);
				send_queue.push(p);
			}
			cout << "BEGIN KEY PROPAGATION" << endl;
		}
	}
}

void BasicDistributedProtocolNode::process(Packet *packet){
	DistributedProtocol::process(packet);
	
	Packet *p;
	
	// 2. determine if the packet requires any behavior from this ECU
	// 3. add any packet ECU needs to send to the send_queue
	if(packet->PDU_format()==JOIN_ASK){
		// stop accepting join\_ask when there is a neighbor
		if(neighbor_v.size()==0){
			joining_node=true;
			// this node will not initiate the SCC
			initiate_timer->cancel();
			// interrogate the node asking this node to join
			p=new InterrogatePacket();
			send_queue.push(p);
			accept_certificate=true;
			cout << "join_ask" << endl;
		}
	}
	if(packet->PDU_format()==CERTIFICATE){
		// if accepting certificates, add a neighbor
		// if the packet has destination address equal to my source address
		if(accept_certificate && 
				packet->PDU_specific()==source_address && 
				packet->verify(TTP_key)){
			accept_certificate=false;
			neighbor_v.push_back(packet->source_address());
			//TODO extract key from certificate
			Key node_public_key=packet->data[0];
			curve_key_map[neighbor_v.back()]=Crypto::curve25519_public_function(private_key, node_public_key);
			if(joining_node){
				joining_node=false;
				// send JOIN_ASK packet
				p=new JoinAskPacket();
				send_queue.push(p);
				// start the last_node_timer incase there is no response
				last_node_timer=new Timer(std::chrono::milliseconds(1000));
				accept_interrogate=true;
			}
			cout << "certificate" << endl;
		}
	}
	// upon interrogation, present certificate
	if(packet->PDU_format()==INTERROGATE){
		if(accept_interrogate){
			accept_interrogate=false;
			last_node_timer->cancel();
			p=new InterrogatePacket(); 
			send_queue.push(p);
			accept_certificate=true;

			p=new CertificatePacket(); 
			p->set_PDU_specific(packet->source_address());
			send_queue.push(p);
		}
		if(joining_node){
			p=new CertificatePacket(); 
			p->set_PDU_specific(packet->source_address());
			send_queue.push(p);
		}
		// last joining node responds to requests to join the secure channel
		if(last_node){
			last_node=false;
			p=new InterrogatePacket(); 
			send_queue.push(p);
			accept_certificate=true;
			p=new CertificatePacket(); 
			p->set_PDU_specific(packet->source_address());
			send_queue.push(p);
		}
		cout << "interrogate" << endl;
	}
	if(packet->PDU_format()==KEY){
		// if the packet has destination address equal to my source address
		if(packet->PDU_specific()==source_address){
			uint64_t neighbor_key=curve_key_map[packet->source_address()];
			packet->decrypt(neighbor_key);
			//TODO extract data from packet
			next_session_key=packet->data[0];
			
			// propagate data to next node
			bool propagated_key=false;
			for(auto const& x : neighbor_v){
				// for the neighbor this node did not receive the key from
				if(x != packet->source_address()){
					p=new KeyPacket();
					p->set_PDU_specific(x);
					p->set_data(next_session_key);
					p->encrypt(curve_key_map[x]);
					send_queue.push(p);
					propagated_key=true;
				}
			}
			if(!propagated_key){
				// there was no neighbor to propagate to
				// this must be the initiator
				p=new ActivateKeyPacket();
				send_queue.push(p);
				activate_key_count++;
			}
			
			cout << "key" << endl;
		}
	}
	if(packet->PDU_format()==ACTIVATE_KEY){
		cout << "activate key" << endl;

		activate_key_count++;
		if(activate_key_count==1 && last_node){
			p=new ActivateKeyPacket(); 
			send_queue.push(p);
			activate_key_count++;
		}
		if(activate_key_count==2){
			session_key=next_session_key;
			for(auto const& x : freshness_map){
				// reset freshness counters
				freshness_map[x.first]=0;
			}
			cout << "KEY ACTIVE" << endl;
		}
	}
	if(packet->PDU_format()==REQUEST_KEY){
		//TODO
		cout << "request_key" << endl;
	}
	if(packet->PDU_format()==RELINK){
		//TODO
		cout << "relink" << endl;
	}
	if(packet->PDU_format()==SYNC_ASK){
		// if this is the last joining node
		if(last_node){
			process_sync_ask(packet);
		}
		cout << "sync_ask" << endl;
	}
}


void BasicDistributedProtocolSyncAskNode::powerup(){
	BasicDistributedProtocolNode::powerup();

	// suppose the node falls out of sync after 5 seconds
	sync_ask_timer=new Timer(std::chrono::milliseconds(5000));
}

void BasicDistributedProtocolSyncAskNode::main(Bus* bus){
	BasicDistributedProtocolNode::main(bus);
	
	// randomize the freshness_map
	for(auto const& x : freshness_map){
		freshness_map[x.first]=rand();
	}

	if(sync_ask_timer && sync_ask_timer->expired){
		sync_ask_timer->cancel();
		send_sync_ask();
	}
}
