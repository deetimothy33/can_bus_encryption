#ifndef DISTRIBUTED_PROTOCOL_H
#define DISTRIBUTED_PROTOCOL_H

#include <map>

#include "Protocol.h"
#include "Timer.h"
#include "Crypto.h"

// common for all distributed protocols
class DistributedProtocol : public BaseProtocol{
	public:
	virtual void main(Bus* bus);
	virtual void process(Packet *packet);
	virtual void powerup();
};

class BasicDistributedProtocolNode : public DistributedProtocol{
	private:
	bool last_node=false;
	bool accept_interrogate=false;
	bool accept_certificate=false;
	bool joining_node=false;
	int activate_key_count=0;
	// map a source address to a curve key
	map<uint8_t,uint64_t> curve_key_map;
	vector<uint8_t> neighbor_v;
	Timer *initiate_timer=0;
	Timer *last_node_timer=0;

	public:
	virtual void main(Bus* bus);
	virtual void process(Packet *packet);
	virtual void powerup();
};


class BasicDistributedProtocolSyncAskNode : public BasicDistributedProtocolNode{
	public:
	Timer *sync_ask_timer;

	virtual void main(Bus* bus);
	virtual void powerup();
};

#endif
