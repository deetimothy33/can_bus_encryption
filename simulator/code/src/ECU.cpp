#include "ECU.h"

ECU::ECU(Protocol* p){
	protocol = p;
}

void ECU::set_bus(Bus* b){
	bus = b;
}

// spawn a thread to begin operating ECU
void ECU::start(){
	stop_ecu_thread = false;
	ecu_thread = new thread(&ECU::main, this);
}

// terminate the operating thread
void ECU::stop(){
	stop_ecu_thread = true;

	// wait for the thread to stop
	ecu_thread->join();
}

void ECU::main(){
	// clock which calls protocol->main() every tick
	auto last_time = Clock::now();

	while(!stop_ecu_thread){
		if(Clock::now() - last_time > chrono::nanoseconds(PERIOD)){
			protocol->main(bus);
			last_time = Clock::now();
		}else{
			this_thread::sleep_for(Clock::now() - last_time - chrono::nanoseconds(1000));	
		}
	}
}

Gateway::Gateway(Protocol* p, int sockets) {
	for(int i=0; i<sockets; i++){
		node_v.push_back(new Node(p));
	}
}

Gateway::~Gateway(){
	// deconstruct each Node
	for(vector<Node*>::iterator it=node_v.begin();
			it!=node_v.end(); ++it){
		delete *it;
	}
}

Node::Node(Protocol* p) : ECU(p){
	// create a CANSocket associated with this ECU	
	//TODO take in a CAN_INTERFACE name
	can_socket = new CANSocket(CAN_INTERFACE);	
	p->can_socket=can_socket;
}

Node::~Node(){
	//TODO should protocol have its can_socket set to nullptr?
	delete can_socket;
}
