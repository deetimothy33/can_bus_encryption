#include "Packet.h"

/* TODO 
 * The following three commented out methods
 * are what the locations of things should be.
 * For some reason, candump utility uses the LSB for when outputting the ID.
 */
/*
uint8_t Packet::source_address(){
	uint32_t mask=0x000000FF;
	return id & mask;
}

uint8_t Packet::PDU_specific(){
	uint32_t mask=0x0000FF00;
	return (id & mask) >> 8;
}

uint8_t Packet::PDU_format(){
	uint32_t mask=0x00FF0000;
	return (id & mask) >> 16;
}
*/

/* therefore,
 * the functions are adjusted to the following
 */
// --------
uint8_t Packet::PDU_format(){
	uint32_t mask=0x000000FF;
	return id & mask;
}

// USED AS DESTINATION ADDRESS
uint8_t Packet::PDU_specific(){
	uint32_t mask=0x0000FF00;
	return (id & mask) >> 8;
}

void Packet::set_PDU_specific(uint8_t dest_address){
	uint32_t mask=0xFFFF00FF;
	uint32_t temp=0;
	temp=dest_address;
	temp=temp<<8;
	// clear old bits
	id=id&mask;
	// place new bits
	id=id|temp;
}

uint8_t Packet::source_address(){
	uint32_t mask=0x00FF0000;
	uint32_t temp=0;
	temp=id & mask;
	temp=temp>>16;
	return temp;
}
	
void Packet::set_source_address(uint8_t source_address){
	uint32_t mask=0xFF00FFFF;
	uint32_t temp=0;
	temp=source_address;
	temp=temp<<16;
	// clear old source address bits
	id=id&mask;
	// place new source address
	id=id|temp;
}
	
	//TODO remove the below
#include <iostream>
	using namespace std;
void Packet::set_data(uint64_t high, uint64_t low){
	// assumes 16 byte data packets
	uint64_t mask=0xFF000000;
	uint64_t temp=0;
	char value=0;
	for(int i=0;i<8;i++){
		// get bits
		mask=mask>>(8*i);
		// put in lsb
		temp=high&mask;
		value=temp>>(56-(8*i));
		data[i]=value;

		temp=low&mask;
		value=temp>>(56-(8*i));
		data[i+8]=value;
	}
	//cout << high << " " << low << endl;
}

void Packet::set_data(uint128_t data){
	// assumes 16 byte data packets
	uint128_t mask=0xFFFFFFFF00000000;
	uint64_t high=(data&mask)>>64;
	uint64_t low=(mask>>64)&data; 
	set_data(high,low);
	
	/*
	uint128_t mask=0xFF00000000000000;
	uint128_t temp=0;
	char value=0;
	for(int i=0;i<16;i++){
		mask=mask>>(8*i);
		// get the bits we want
		temp=data&mask;
		// put the bits in the LSB
		value=temp>>(120-(8*i));
		data[i]=value;
	}
	*/
}
// --------

uint8_t Packet::data_page(){
	uint32_t mask=0x01000000;
	return (id & mask) >> 24;
}

uint8_t Packet::extended_data_page(){
	uint32_t mask=0x02000000;
	return (id & mask) >> 25;
}

uint8_t Packet::priority(){
	uint32_t mask=0x1C000000;
	return (id & mask) >> 26;
}


BasePacket::BasePacket(){
	type=CANFD;
	id = 0;
	data_length=16;
	flags=0;
	reserved_0=0;
	reserved_1=0;
	for(int i=0;i<CANFD_MAX_DLEN;i++) data[i]=0;
}

BasePacket::~BasePacket(){}

void BasePacket::encrypt(Key key){}
void BasePacket::decrypt(Key key){}

void BasePacket::sign(Key key){}
bool BasePacket::verify(Key key){ return true; }


SecurePacket::SecurePacket(){
	type = CANFD;
}

void SecurePacket::sign(Key key){
	// AES-CMAC(key, message || counter);
	// tag is the 32 LSB of the MAC
	secure_packet_data.tag = Crypto::mask(0,31) & Crypto::aes_cmac(key, secure_packet_data.mctr);
	//TODO make sure passing mctr works as expected. I could Crypto::mask it instead
}

bool SecurePacket::verify(Key key){
	// CMAC(key, secure_packet_data.mctr) == secure_packet_data.tag;
	// tag is the 32 LSB of the MAC
	//TODO
	return true;
	//return (Crypto::aes_cmac(key, secure_packet_data.mctr) & Crypto::mask(0,31)) == secure_packet_data.tag;;
}

CertificatePacket::CertificatePacket(){
	id=CERTIFICATE;
}
	
bool CertificatePacket::verify(Key key){
	// ED25519(key, certificate_packet_data.certificate) == certificate_packet_data.signature;
	//TODO
	return true;
	//return Crypto::ed25519(key, certificate_packet_data.certificate) == certificate_packet_data.signature;
}

// KEY
KeyPacket::KeyPacket(){
	id=KEY;
}

void KeyPacket::encrypt(Key key){
	// CURVE25519-ENCRYPT(key, *data); 
	*data = Crypto::curve25519_encrypt(key, *data);
	//TODO does this assignment work?
	//	perhaps I will need to do each byte individually?
}

void KeyPacket::decrypt(Key key){
	// CURVE25519-DECRYPT(key, *data); 
	*data = Crypto::curve25519_decrypt(key, *data);
	//TODO same assignment question
}

// JOIN_ASK
JoinAskPacket::JoinAskPacket(){
	id=JOIN_ASK;
}

// OK
OKPacket::OKPacket(){
	id=OK;
}

// INTERROGATE
InterrogatePacket::InterrogatePacket(){
	id=INTERROGATE;
}

// SYNC_ASK
SyncAskPacket::SyncAskPacket(){
	id=SYNC_ASK;
}

// ACTIVATE_KEY
ActivateKeyPacket::ActivateKeyPacket(){
	id=ACTIVATE_KEY;
}

// FRESHNESS
FreshnessPacket::FreshnessPacket(){
	id=FRESHNESS;	
}

// REQUEST_KEY
RequestKeyPacket::RequestKeyPacket(){
	id=REQUEST_KEY;	
}

// MASTER_CERTIFICATE
MasterCertificatePacket::MasterCertificatePacket(){
	id=MASTER_CERTIFICATE;	
}

// RELINK
RelinkPacket::RelinkPacket(){
	id=RELINK;	
}

// DATA
DataPacket::DataPacket(){
	id=DATA;
}
