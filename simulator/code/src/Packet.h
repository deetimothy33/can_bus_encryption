#ifndef PACKET_H
#define PACKET_H

using namespace std;

#include <linux/can.h>
#include "Crypto.h"

enum PacketType { CAN, CANFD };

//TODO id values for differnt message types
//	values are 8 bits
//	the shift puts them in the PDU bit places
//	identifiers are 29 bits
enum ID { 
	JOIN_ASK=0, //(0x00<<16),
	ACTIVATE_KEY,
	FRESHNESS,
	KEY,
	SYNC_ASK,
	OK,
	INTERROGATE,
	CERTIFICATE,
	REQUEST_KEY,
	MASTER_CERTIFICATE,
	RELINK,
	DATA
};

/**
 * Packets are transmitted on the CANbus
 * different packets exist for different types of CAN protocols (CAN-FD, CAN...)
 *
 * This is a wrapper around 
 * 	can_frame
 * 	canfd_frame
 * defined in /include/uapi/linux/can.h
 */

class Packet{
	public:
	PacketType type;

	// set true upon receiving packet 
	// (and populating data)
	bool populated=false;

	// id's are a property of the packet / message
	uint32_t id;
	uint8_t data_length;
	uint8_t flags; // padding for normal can_frame, flags for canfd_frame
	uint8_t reserved_0;
	uint8_t reserved_1;
	unsigned char data[CANFD_MAX_DLEN];
	
	virtual ~Packet() =0;

	// extract bits from identifier
	// 8 bit
	uint8_t source_address();
	// 8 bit
	uint8_t PDU_specific();
	// 8 bit
	uint8_t PDU_format();
	// 1 bit
	uint8_t data_page();
	// 1 bit
	uint8_t extended_data_page();
	// 3 bit
	uint8_t priority();

	// set identifier bits
	void set_source_address(uint8_t source_address);
	void set_PDU_specific(uint8_t dest_address);
	void set_data(uint64_t high, uint64_t low);
	void set_data(uint128_t data);

	// encrypt or decrypt (the current data bits)
	virtual void encrypt(Key key) =0;
	virtual void decrypt(Key key) =0;

	// sign or verify the message data (modifies the data field)
	virtual void sign(Key key) =0;
	// if CERTIFICATE, then verify using TTP_key
	// if DATA, then verify mac with session_key
	virtual bool verify(Key key) =0;
};	
inline Packet::~Packet(){}

class BasePacket : public Packet{
	public:
	BasePacket();
	virtual ~BasePacket();

	virtual void encrypt(Key key);
	virtual void decrypt(Key key);
	
	virtual void sign(Key key);
	virtual bool verify(Key key);
};

// DATA
union SecurePacketData{
	uint128_t data;
	struct{
		uint64_t message;
		uint32_t counter;
		uint32_t tag;
	};
	struct{
		//TODO will this do what I think it does?
		//	namely, will accessing this get the first 96 bits of data?
		uint128_t mctr : 96;
	};
};

class SecurePacket : public BasePacket{
	SecurePacketData secure_packet_data;
	
	public:
	SecurePacket();
	
	virtual void sign(Key key);
	virtual bool verify(Key key);
};

// CERTIFICATE
//TODO update this to be useful
//TODO the bitfields do not necessarily reflect X.509 certificates yet
union CertificatePacketData{
	uint128_t data;
	//TODO does this do what I think it does? 
	//	I want the first 96 bits to be the certificate
	//	the last 32 bits to be the signature
	struct{
		uint128_t certificate : 96;
		uint32_t signature; 
	};
	//TODO update to reflect the location of the public key
	struct{
		uint64_t junk;
		uint32_t public_key;
	};
};

class CertificatePacket : public BasePacket{
	CertificatePacketData certificate_packet_data;

	public:
	CertificatePacket();
	
	virtual bool verify(Key key);	
};

// KEY
class KeyPacket: public BasePacket{
	public:
	KeyPacket();
	
	virtual void encrypt(Key key);
	virtual void decrypt(Key key);
};

// JOIN_ASK
class JoinAskPacket: public BasePacket{
	public:
	JoinAskPacket();
};

// OK
class OKPacket: public BasePacket{
	public:
	OKPacket();
};

// INTERROGATE
class InterrogatePacket: public BasePacket{
	public:
	InterrogatePacket();
};

// SYNC_ASK
class SyncAskPacket: public BasePacket{
	public:
	SyncAskPacket();
};

// ACTIVATE_KEY
class ActivateKeyPacket: public BasePacket{
	public:
	ActivateKeyPacket();
};

// FRESHNESS
class FreshnessPacket: public BasePacket{
	public:
	FreshnessPacket();
};

// REQUEST_KEY
class RequestKeyPacket: public BasePacket{
	public:
	RequestKeyPacket();
};

// MASTER_CERTIFICATE
class MasterCertificatePacket: public CertificatePacket{
	public:
	MasterCertificatePacket();
};

// RELINK
class RelinkPacket: public BasePacket{
	public:
	RelinkPacket();
};

// DATA
class DataPacket : public SecurePacket{
	public:
	DataPacket();
};

#endif
