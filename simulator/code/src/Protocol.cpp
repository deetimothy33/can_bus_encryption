#include "Protocol.h"
	
BaseProtocol::BaseProtocol(){
	// initial key,freshness generation
	//srand(12039487); -- don't want all nodes to have the same source address, therefore no seed
	//for(int i=0;i<FRESHNESS_BYTES;++i) freshness[i]=rand();
	//for(int i=0;i<KEY_BYTES;++i) key[i]=rand();
	
	// generate a random source address
	source_address=rand()%0x100;
	// arbitrary 64-bit key
	TTP_key=0xAABBCCDDEEFF1122; 
	//TODO in reality this would need to match the certificate
	private_key=rand();
}

void BaseProtocol::powerup(){
	// send some dummy data
	dummy_data_timer=new Timer(std::chrono::milliseconds((rand()%400)+100));	
}

// common parts of main function, subclasses can call 'super'.main()
void BaseProtocol::main(Bus* bus){
	Packet* p;
	
	// call function once at power up
	if(powerup_b){ 
		powerup(); 
		powerup_b=false;
	}
	
	// send
	if(!send_queue.empty()){
		// if there are messages to transmit, transmit them
		// other behaviors will add messages to the queue
		if(transmit(send_queue.front(), bus)){
			delete send_queue.front();
			send_queue.pop();
		}
	}
	
	// receive
	if(!receive_queue.empty()){
		// process received packet
		process(receive_queue.front());
		delete receive_queue.front();
		receive_queue.pop();
	}

	// attempt a non_blocking read from the can_socket
	// 	only add to the receive queue on packet receipt
	p=new BasePacket();
	if(receive(p, bus)) receive_queue.push(p);
	else delete p;
	
	// send dummy data
	if(dummy_data_timer && dummy_data_timer->expired){
		p=new DataPacket();
		send_queue.push(p);
		dummy_data_timer->reset();
	}
}

// use Node's CANSocket
//	(to which this protocol is assigned) to
//	1. transmit packets
//	2. receive packets
// create a set_socket() method to Protocol
// check whether the socket is NULL before trying to use it
// this way there will simply be nothing sent if the set_socket() method is never called

bool BaseProtocol::transmit(Packet* packet, Bus* bus){
	// tell other nodes to receive
	//bus->sync(true);
	//this_thread::sleep_for(chrono::milliseconds(100));
	//bus->sync(false);

	// add source address to packet
	packet->set_source_address(source_address);

	// write to the underlying CAN infrastructure
	if(can_socket) *can_socket << *packet;

	// write to the bus so it can keep metrics
	*bus << *packet;

	//cout << "tx" << endl;

	return true;
};

// add any packet transmitted on the bus to the receive_queue
bool BaseProtocol::receive(Packet* packet, Bus* bus){
	// wait for sync to finish
	//while(bus->sync()) this_thread::sleep_for(chrono::microseconds(10));

	// read from the underlying CAN infrastructure
	if(can_socket) *can_socket >> *packet;

	// determine if a packet was received
	if(packet->populated){
		// read from the bus so it can keep metrics
		*bus >> *packet;
	}

	return packet->populated;
};

void BaseProtocol::process(Packet* packet){
	if(packet->PDU_format()==FRESHNESS){
		//TODO extract actual freshness counter value
		//	data[1] will be changed
		freshness_map[packet->data[0]]=packet->data[1];
		cout << "freshness" << endl;
	}
		
	if(packet->PDU_format()==DATA){
		// test the freshness value
		if(false){
			// for incoreect freshness
			send_sync_ask();
		}
		// increment node freshness
		freshness_map[packet->source_address()]++;

		//cout << "data " << freshness_map[packet->source_address()] << endl;
	}
};

void BaseProtocol::send_sync_ask(){
	Packet *p;

	p=new SyncAskPacket();

	//int bits_per_counter=floor(freshness_map.size()/128);
	int i=0;
	for(auto const& x : freshness_map){
		//TODO for simplicity -- I will just use 1 byte for counter
		//	this should be changed to bits_per_counter
		p->data[i]=Crypto::hash(x.second);// % (1<<bits_per_counter);
		i++;
	}
	
	send_queue.push(p);
}

void BaseProtocol::process_sync_ask(Packet *p){
	cout << "processing" << endl;

	//int bits_per_counter=floor(freshness_map.size()/128);
	int i=0;
	for(auto const& x : freshness_map){
		// test if packet data matches freshness hash
		if(p->data[i]!=Crypto::hash(x.second)){// % (1<<bits_per_counter)){
			p=new FreshnessPacket(); 
			//TODO populate FRESHNESS with source address x and counter
			p->data[0]=x.first; 
			// 1<<x is 2^x
			//TODO change which data bytes get data
			//	this should just be freshness_map[x.second];
			p->data[1]=freshness_map[x.second]; //% (1<<bits_per_counter);
			send_queue.push(p);
		}
		i++;
	}
}
