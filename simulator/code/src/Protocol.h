#ifndef PROTOCOL_H
#define PROTOCOL_H

class Packet;
class Bus;

#include <iostream>
#include <queue>
#include <map>
#include <thread>
#include <cmath>
#include "Bus.h"
#include "Packet.h"
#include "macro.h"
#include "CANSocket.h"
#include "Crypto.h"
#include "Timer.h"

using namespace std;

// allows for redefinition of
// session key
// freshness value
// without changing much code
//#define KEY_BYTES 8
//#define FRESHNESS_BYTES 8
//typedef uint64_t session_key_t;
// hash map source addresses to freshness values
typedef map<uint8_t,uint64_t> freshness_t;

// the frequency at which Nodes test freshness to ensure it is in-sync
// 	in seconds
#define FRESHNESS_TEST_INTERVAL 30


/**
 * Protocols define how ECU operates.
 * The purpose of doing it this way is to make the protocols used clear and easy to read.
 * In construction CAN bus, passing different protocols to ECU will change how they act.
 *
 * The protocols can also be broken down into small peices.
 * This prevents duplication of code bits.
 * Protocols only need to subclass the BaseProtocol and implement the portions needed.
 */

class Protocol{
	public:
	// associates a can_socket with this protocol
	CANSocket *can_socket=nullptr;

	// function will be executed in a loop by ECUs
	// main describes the behavior of the ECU
	// behavior := when to call the other functions defined by the protocol
	virtual void main(Bus*) =0;

	// true if transmission was successful
	virtual bool transmit(Packet* packet, Bus* bus) =0;
	// true if a packet is received
	virtual bool receive(Packet* packet, Bus* bus) =0;
	virtual void process(Packet* packet) =0;
	// called once at powerup
	virtual void powerup() =0;
};

// things common for Centeralized and Distributed Protocols
class BaseProtocol : public Protocol{
	bool powerup_b=true;
	queue<Packet*> receive_queue;
	Timer *dummy_data_timer;

	public:
	uint8_t source_address;
	uint64_t TTP_key;
	uint64_t private_key;
	freshness_t freshness_map;
	Key session_key;
	Key next_session_key;
	queue<Packet*> send_queue;

	BaseProtocol();

	// common parts of main function, subclasses can call 'super'.main()
	virtual void main(Bus* bus);	

	virtual bool transmit(Packet* packet, Bus* bus);
	virtual bool receive(Packet* packet, Bus* bus);
	// process received packets
	virtual void process(Packet* packet);
	virtual void powerup();

	// build and send a SYNC_ASK packet to the given source address
	virtual void send_sync_ask();
	virtual void process_sync_ask(Packet *p);
};


#endif
