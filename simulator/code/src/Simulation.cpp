#include "Simulation.h"

Simulation::Simulation(Bus* b){
	bus = b;
}

void Simulation::print_metrics(){
	// print all possible metrics from the bus
	cout << "-------------" << endl;
	cout << bus->metric << endl;
}
	

TestSimulation::TestSimulation(Bus* b) : Simulation(b){}

void TestSimulation::run(){
	// change this to change between:
	//	true -- centralized
	//	false -- distributed
	bool centralized=false;

	if(centralized){
		// CENTRALIZED
		bus->attach(new Node(new BasicCenteralizedProtocolMaster()));
		bus->attach(new Node(new BasicCenteralizedProtocolNode()));
		//bus->attach(new Node(new BasicCenteralizedProtocolNode()));
	}else{
		// DISTRIBUTED
		bus->attach(new Node(new BasicDistributedProtocolNode()));
		bus->attach(new Node(new BasicDistributedProtocolNode()));
	}

	bus->power_up();
	this_thread::sleep_for(chrono::seconds(3));
	bus->power_down();
}


DistributedKeyNegotiationSimulation::DistributedKeyNegotiationSimulation(Bus *b) : Simulation(b){}

void DistributedKeyNegotiationSimulation::run(){
	bus->attach(new Node(new BasicDistributedProtocolNode()));
	bus->attach(new Node(new BasicDistributedProtocolNode()));
	
	//TODO with three nodes, the key keeps getting propagated indeffonatly
	//	are they all becoming neighbors of one-another?
	bus->attach(new Node(new BasicDistributedProtocolNode()));
	
	bus->power_up();
	this_thread::sleep_for(chrono::seconds(8));
	bus->power_down();
}


CentralizedKeyNegotiationSimulation::CentralizedKeyNegotiationSimulation(Bus *b) : Simulation(b){}

void CentralizedKeyNegotiationSimulation::run(){
	bus->attach(new Node(new BasicCenteralizedProtocolMaster()));
	bus->attach(new Node(new BasicCenteralizedProtocolNode()));
	bus->attach(new Node(new BasicCenteralizedProtocolNode()));
	
	bus->power_up();
	this_thread::sleep_for(chrono::seconds(6));
	bus->power_down();
}


DistributedFreshnessSynchronizationSimulation::DistributedFreshnessSynchronizationSimulation(Bus *b) : Simulation(b){}

void DistributedFreshnessSynchronizationSimulation::run(){
	bus->attach(new Node(new BasicDistributedProtocolNode()));
	bus->attach(new Node(new BasicDistributedProtocolSyncAskNode()));
	
	bus->power_up();
	this_thread::sleep_for(chrono::seconds(8));
	bus->power_down();
}


CentralizedFreshnessSynchronizationSimulation::CentralizedFreshnessSynchronizationSimulation(Bus *b) : Simulation(b){}

void CentralizedFreshnessSynchronizationSimulation::run(){
	bus->attach(new Node(new BasicCenteralizedProtocolMaster()));
	bus->attach(new Node(new BasicCenteralizedProtocolNode()));
	bus->attach(new Node(new BasicCenteralizedProtocolSyncAskNode()));
	
	bus->power_up();
	this_thread::sleep_for(chrono::seconds(8));
	bus->power_down();
}
