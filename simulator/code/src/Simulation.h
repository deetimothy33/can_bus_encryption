#ifndef SIMULATION_H
#define SIMULATION_H

#include <iostream>
#include <chrono>
#include "Bus.h"
#include "macro.h"
#include "CenteralizedProtocol.h"
#include "DistributedProtocol.h"

using namespace std;

/**
 * Simulations
 * Run a preset scenario on the given bus
 */

class Simulation{	
	protected:
	Bus* bus;

	public:
	Simulation(Bus* b);

	void print_metrics();

	virtual void run() =0;
};

class XMLSimulation : public Simulation{
	private:
	void load_simulation_xml(string filename);
	
	public:
	XMLSimulation(Bus* b, string xml_filename);

	void run();
};

class TestSimulation : public Simulation{
	public:
	TestSimulation(Bus* b);

	void run();
};

class DistributedKeyNegotiationSimulation : public Simulation{
	public:
	DistributedKeyNegotiationSimulation(Bus* b);

	void run();
};

class CentralizedKeyNegotiationSimulation : public Simulation{
	public:
	CentralizedKeyNegotiationSimulation(Bus* b);

	void run();
};

class DistributedFreshnessSynchronizationSimulation : public Simulation{
	public:
	DistributedFreshnessSynchronizationSimulation(Bus* b);

	void run();
};

class CentralizedFreshnessSynchronizationSimulation : public Simulation{
	public:
	CentralizedFreshnessSynchronizationSimulation(Bus* b);

	void run();
};

#endif
