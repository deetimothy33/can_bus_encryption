#include "Timer.h"

Timer::Timer(std::chrono::milliseconds milliseconds){
	initial_duration=milliseconds;
	reset();
}

#include <iostream>
void Timer::reset(){
	remaining_duration=initial_duration;
	canceled=false;
	expired=false;

	// test if thread is still running
	if(done){
		// old thread finished
		//if(timer_thread) delete timer_thread;
		timer_thread=new thread(&Timer::trigger,this);
	}
	//ELSE old thread is still running
	// resetting the initial duration is sufficient
}

void Timer::cancel(){
	expired=false;
	canceled=true;
}

void Timer::trigger(){
	// sleep while waiting for time to expire
	while(remaining_duration > std::chrono::milliseconds(0)){
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		remaining_duration-=std::chrono::milliseconds(10);
	}

	if(!canceled) expired=true;
	done=true;
}
