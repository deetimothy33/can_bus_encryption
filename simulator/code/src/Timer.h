#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <thread>

using namespace std;

class Timer{
	private:
	bool canceled=false;
	std::chrono::milliseconds initial_duration;
	std::chrono::milliseconds remaining_duration;
	thread *timer_thread=0;
	// is the thread done?
	bool done=true;

	public:
	// set to true when the timer expires
	bool expired=false;

	Timer(std::chrono::milliseconds milliseconds);

	// reset the timer to the initial duration
	void reset();
	// cancel the timer, 
	// expired will never be set to true
	void cancel();
	
	// create a separate thread
	// sleep for duration
	// set expired to true if the timer has not been canceled or reset
	void trigger();
};

#endif
