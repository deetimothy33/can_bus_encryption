#ifndef MACRO_H
#define MACRO_H

#include <iostream>

using namespace std;

#define CAN_INTERFACE "vcan0"

#define ERROR(string) cerr << "ERROR: " << string << endl;
#define PRINT(string) cout << string << endl;

#endif
