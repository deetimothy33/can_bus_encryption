#include <iostream>
#include <string>
#include <stdlib.h>

#include "Bus.h"
#include "Simulation.h"

using namespace std;

/**
 * BASIC DESIGN
 * Node are connected by Bus
 * Gateways have multiple Nodes
 * 	Gateway Nodes can be connected to different busses
 */

/**
 * main
 * create a bus
 * run simulation on that bus
 */

int main(int argc, char **argv){
	Bus bus;
	Simulation *simulation;
	
	// test the arguments
	if(argc<2){
		cerr << "Please choose a simulation:" << endl;
		cerr << "(0) Test" << endl;
		cerr << "(1) Distributed Key Negotiation" << endl;
		cerr << "(2) Centralized Key Negotiation" << endl;
		cerr << "(3) Distributed Freshness Synchronization" << endl;
		cerr << "(4) Centralized Freshness Synchronization" << endl;
		return 1;
	}

	int sim_i=atoi(argv[1]);

	switch(sim_i){
		case 1: simulation = new DistributedKeyNegotiationSimulation(&bus); 
			cout << "Distributed Key Negotiation" << endl;
			break;
		case 2: simulation = new CentralizedKeyNegotiationSimulation(&bus); break;
			cout << "Centralized Key Negotiation" << endl;
			break;
		case 3: simulation = new DistributedFreshnessSynchronizationSimulation(&bus); break;
			cout << "Distributed Freshness Synchronization" << endl;
			break;
		case 4: simulation = new CentralizedFreshnessSynchronizationSimulation(&bus); break;
			cout << "Centralized Freshness Synchronization" << endl;
			break;
		default: simulation = new TestSimulation(&bus);
			cout << "TEST" << endl;
	}

	//cout << "instantiating protocols" << endl;
	//cout << "creating can bus" << endl;
	//cout << "running simulation" << endl;
	simulation->run();

	//cout << "printing metrics" << endl;
	//simulation->print_metrics();

	cout << "COMPLETE" << endl;

	return 0;
}

/**
 * notes
 * includes: section 4, http://www.cplusplus.com/forum/articles/10627/
 * crypto++565 library for cryptography
 */

/**
 * related projects
 * https://github.com/linux-can/can-utils
 * https://github.com/zombieCraig/ICSim 
 */

/**
 * utilizes SocketCAN a set of open source CAN drivers and a networking stack in the Linux kernel.
 * can-utils package provides this.
 * can-utils utilizes SocketCAN
 *
 * SocketCAN is the offical CAN API of the linux kernel
 */
