var hierarchy =
[
    [ "BaseCrypto", "class_base_crypto.html", [
      [ "RealCrypto", "class_real_crypto.html", null ],
      [ "SimulatedCrypto", "class_simulated_crypto.html", null ]
    ] ],
    [ "CertificatePacketData", "union_certificate_packet_data.html", null ],
    [ "ECU", "class_e_c_u.html", [
      [ "Gateway", "class_gateway.html", [
        [ "Node", "class_node.html", null ]
      ] ]
    ] ],
    [ "GUI", "class_g_u_i.html", null ],
    [ "high_resolution_clock", null, [
      [ "Clock", "class_clock.html", null ]
    ] ],
    [ "iostream", null, [
      [ "Bus", "class_bus.html", null ],
      [ "CANSocket", "class_c_a_n_socket.html", null ]
    ] ],
    [ "Metric", "struct_metric.html", null ],
    [ "Packet", "class_packet.html", [
      [ "BasePacket", "class_base_packet.html", [
        [ "CANAuthPacket", "class_c_a_n_auth_packet.html", null ],
        [ "CertificatePacket", "class_certificate_packet.html", null ],
        [ "KeyDistributionPacket", "class_key_distribution_packet.html", null ],
        [ "KeyNegotiationPacket", "class_key_negotiation_packet.html", null ],
        [ "SecurePacket", "class_secure_packet.html", null ]
      ] ]
    ] ],
    [ "Protocol", "class_protocol.html", [
      [ "BaseProtocol", "class_base_protocol.html", [
        [ "CenteralizedProtocol", "class_centeralized_protocol.html", [
          [ "C1", "class_c1.html", null ]
        ] ],
        [ "DistributedProtocol", "class_distributed_protocol.html", [
          [ "D1", "class_d1.html", null ]
        ] ],
        [ "MalfunctionProtocol", "class_malfunction_protocol.html", [
          [ "M1", "class_m1.html", null ]
        ] ],
        [ "XMLProtocol", "class_x_m_l_protocol.html", null ]
      ] ]
    ] ],
    [ "RectangleShape", null, [
      [ "GUIComponent", "class_g_u_i_component.html", [
        [ "NodeDisplay", "class_node_display.html", null ],
        [ "Terminal", "class_terminal.html", null ],
        [ "ViewComponent", "class_view_component.html", null ]
      ] ]
    ] ],
    [ "SecurePacketData", "union_secure_packet_data.html", null ],
    [ "Simulation", "class_simulation.html", [
      [ "TestSimulation", "class_test_simulation.html", null ],
      [ "XMLSimulation", "class_x_m_l_simulation.html", null ]
    ] ],
    [ "View", null, [
      [ "ViewComponent", "class_view_component.html", null ]
    ] ]
];