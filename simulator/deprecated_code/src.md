#src
###main.cpp
Sets up the following
	1. a simulated network
	2. a simulated attacker performing replay attacks
#####network
simulates the operations of a CAN bus
the protocols used are defined in XML files
#####attacker
uses can-utils utilities to simulate replay attacks
inserts random packets into the network to see if they mess the network up
