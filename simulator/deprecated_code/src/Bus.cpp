#include "Bus.h"

ostream& operator<<(ostream& os, const Metric& metric){
	return os << "security_messages: " << metric.security_messages << endl
		<< "total_messages: " << metric.total_messages << endl
		<< "freshness_messages: " << metric.freshness_messages << endl
		<< "key_messages: " << metric.key_messages << endl
		<< "error_messages: " << metric.error_messages << endl;
}

//TODO perhaps the Bus could display what is happening graphically? (or a subclass could provide graphics?)
//TODO implement some method for changing the bus architecture
Bus::Bus(){
	state.drivers = 0;
	state.sync = false;
}

void Bus::drive(){
	state.drivers_mutex.lock();
	state.drivers++;
	state.drivers_mutex.unlock();
}

void Bus::undrive(){
	state.drivers_mutex.lock();
	state.drivers--;
	state.drivers_mutex.unlock();
}
	
void Bus::sync(bool s){
	state.drivers_mutex.lock();
	state.sync = s;
	state.drivers_mutex.unlock();
}

int Bus::drivers(){
	state.drivers_mutex.lock();
	int d = state.drivers;
	state.drivers_mutex.unlock();

	return d;
}

bool Bus::sync(){
	state.drivers_mutex.lock();
	bool s = state.sync;
	state.drivers_mutex.unlock();

	return s;
}

void Bus::power_up(){
	for(vector<ECU*>::iterator it = ecu_vector.begin(); it != ecu_vector.end(); ++it){
		(*it)->start();
	}
}

void Bus::power_down(){
	for(vector<ECU*>::iterator it = ecu_vector.begin(); it != ecu_vector.end(); ++it){
		(*it)->stop();
	}
}

//TODO allow attaching at a particular locaion
void Bus::attach(ECU* ecu){
	ecu_vector.push_back(ecu);
	ecu->set_bus(this);
}
	
/**
 * nodes will stream packets to the bus as well as to their CANSockets
 * Bus can keep metrics on the packets sent
 */

Bus& Bus::operator<<(Packet& packet){
	//can_socket << packet;
	//TODO
	
	return *this;
}

Bus& Bus::operator>>(Packet& packet){
	//can_socket >> packet;
	//TODO
	
	return *this;
}

//TODO the ECU could all stop and wait for the bus to advance them one clock tick
//TODO or ECU could have individual clocks and act on the buss as a buffer
//TODO or Bus tells each ECU how much to advance in time?
// --
// The chosen solution (for now) is to have a clock on the ECU running a main() loop	

//TODO methods to report specific metrics based on struct metric record
