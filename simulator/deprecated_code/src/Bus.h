#ifndef BUS_H
#define BUS_H

class ECU;

#include <iostream>
#include <vector>
#include <mutex>
#include <ostream>
#include "ECU.h"

using namespace std;

typedef struct{
	int security_messages;
	int total_messages;
	int freshness_messages;
	int key_messages;
	int error_messages;
	//TODO messages / unit time
	//TODO real-time guarentees
} Metric;
ostream& operator<<(ostream& os, const Metric& metric);

//TODO bus should have a CANSocket
//TODO writing to and reading from the bus should send Packets over the CANSocket
//
//TODO Actually, this may not be the way
//!!!!TODO perhaps each ECU should get its own CANSocket?

/**
 * Bus is a collection of ECU
 * it can be used to perform actions on all ECU's
 */

class Bus : public iostream{
	private:	
	struct{
		int drivers;
		bool sync;
		
		mutex drivers_mutex;
		mutex sync_mutex;
	} state;	
	
	vector<ECU*> ecu_vector;
	//CANSocket can_socket;

	public:	
	Metric metric;

	Bus();

	// methods used by ECU
	void drive();
	void undrive();
	void sync(bool);

	int drivers();
	bool sync();

	// methods used by simulator
	void power_up();
	void power_down();
	void attach(ECU* ecu);
	
	// methods for sending a packet on the bus
	// the packet is sent over the CANSocket
	Bus& operator<<(Packet& packet);
	Bus& operator>>(Packet& packet);
};

#endif
