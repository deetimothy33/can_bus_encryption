#include "CANSocket.h"

//TODO improve this code
CANSocket::CANSocket(string interface){
	const int canfd_on = 1;
	struct ifreq ifr;
	struct sockaddr_can addr;
	//string interface = CAN_INTERFACE;

	// Create a new raw CAN socket
	can = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if(can < 0) ERROR("Couldn't create raw socket");

	addr.can_family = AF_CAN;
	memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
	strncpy(ifr.ifr_name, interface.c_str(), strlen(interface.c_str()));

	//strncpy(ifr.ifr_name, argv[optind], strlen(argv[optind]));
	strncpy(ifr.ifr_name, interface.c_str(), interface.length());
	//copy(interface.c_str()[0], interface.c_str()+interface.length(), ifr.ifr_name);
	printf("Using CAN interface %s\n", ifr.ifr_name);

	if (ioctl(can, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		exit(1);
	}

	addr.can_ifindex = ifr.ifr_ifindex;
	// CAN FD Mode
	setsockopt(can, SOL_CAN_RAW, CAN_RAW_FD_FRAMES, &canfd_on, sizeof(canfd_on));

	//TODO figure out what the following two blocks of code do
	
	struct iovec iov;

	// sys/socket.h
	// CMSG_SPACE() returns the number of bytes an ancillary element with payload of the passed data length occupies. This is a constant expression.
	//char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
	iov.iov_base = &cf;
	iov.iov_len = sizeof(cf);

	// options for message?
	msg.msg_name = &addr;
	msg.msg_namelen = sizeof(addr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	
	//TODO
	//msg.msg_control = &ctrlmsg;
	//msg.msg_controllen = sizeof(ctrlmsg);
	
	msg.msg_flags = 0;

	if (bind(can, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		exit(1);
	}
}

CANSocket::~CANSocket(){
	close(can);
}

CANSocket& CANSocket::operator<<(Packet& packet){
	int mtu;

	// clear cf buffer
	memset(&cf, 0, sizeof(cf));

	// populate can frame with values from packet
	cf.can_id = packet.id;
	cf.len = packet.data_length;
	cf.flags = packet.flags;
	cf.__res0 = packet.reserved_0;
	cf.__res1 = packet.reserved_1;
	copy(packet.data, packet.data+packet.data_length, cf.data);
		
	mtu = (packet.type==CAN) ? CAN_MTU : CANFD_MTU;
	send_pkt(can, &cf, mtu);
	
	return *this;
}

CANSocket& CANSocket::operator>>(Packet& packet){	
	//TODO how do I tell how many bytes the packet is?
	receive_pkt(can, &cf, CAN_MTU);

	// populate packet fields from can frame
	packet.id = cf.can_id;
	packet.data_length = cf.len;
	packet.flags = cf.flags;
	packet.reserved_0 = cf.__res0;
	packet.reserved_1 = cf.__res1;
	copy(cf.data, cf.data+cf.len, packet.data);

	/*
	cout << "id " << cf.can_id << endl
		<< "len " << cf.len << endl
		<< "data[0] " << cf.data[0] << endl
		<< "data[1] " << cf.data[1] << endl
		<< "data[2] " << cf.data[2] << endl
		<< "data[3] " << cf.data[3] << endl;
	*/

	return *this;
}

//TODO make sure std::copy is working as expected

void send_pkt(int can, const struct canfd_frame *cf, int mtu){
	// s = socket(...)
	//if(write(s, &cf, mtu) != mtu) {
	if(write(can, cf, mtu) != mtu) {
		perror("write");
	}
}

void receive_pkt(int can, struct canfd_frame *cf, int mtu){
	//int maxdlen;
	//struct cmsghdr *cmsg;
	//struct timeval tv;
	int nbytes;

	// ssize_t recvmsg(int sockfd, struct msghdr *msg, int flags);
	//nbytes = recvmsg(can, &msg, 0);
	nbytes = read(can, cf, CANFD_MTU); //TODO what MTU

	if (nbytes < 0){
		perror("read");
	}else if ((size_t)nbytes == CAN_MTU){
		//maxdlen = CAN_MAX_DLEN;
	}else if ((size_t)nbytes == CANFD_MTU){
		//maxdlen = CANFD_MAX_DLEN;
	}else{
		fprintf(stderr, "read: incomplete CAN frame\n");
	}
	
	//TODO this works with the auxiliary information from recvmsg
	/*
	for (cmsg = CMSG_FIRSTHDR(&msg);
			cmsg && (cmsg->cmsg_level == SOL_SOCKET);
			cmsg = CMSG_NXTHDR(&msg,cmsg)) {
		if (cmsg->cmsg_type == SO_TIMESTAMP)
			tv = *(struct timeval *)CMSG_DATA(cmsg);
		else if (cmsg->cmsg_type == SO_RXQ_OVFL)
			//dropcnt[i] = *(__u32 *)CMSG_DATA(cmsg);
			fprintf(stderr, "Dropped packet\n");
	}
	*/
}
