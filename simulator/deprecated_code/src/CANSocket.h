#ifndef CANSOCKET_H
#define CANSOCKET_H

class Packet;

#include <iostream>
#include <algorithm>
#include "Packet.h"
#include "macro.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include "lib.h"

using namespace std;

/**
 * /include/uapi/linux/can.h 
 * line 131
 *
 * struct canfd_frame - CAN flexible data rate frame structure
 * @can_id: CAN ID of the frame and CAN_*_FLAG flags, see canid_t definition
 * @len:    frame payload length in byte (0 .. CANFD_MAX_DLEN)
 * @flags:  additional flags for CAN FD
 * @__res0: reserved / padding
 * @__res1: reserved / padding
 * @data:   CAN FD frame payload (up to CANFD_MAX_DLEN byte)
 */

/**
 * CANSocket
 * Picture a socket like a stream on which you send packets.
 *
 * abstracts away all interactions with the vcan0 interface
 * note that this can (heh) also be used to communicate with
 * real can devices.
 * vcan0 uses SocketCAN, the can drive built into the linux kernel
 * it also works for real CAN cards.
 *
 * canfd_frame struct is used for both can and CAN and CANFD frames
 * this is because the contents of a CANFD frame 
 * can (heh) be thought of as a superset of the CAN frame contents
 *
 * (send_pkt) will only send CAN_MAX_DLEN from buffer for frames intended to be CAN 
 */

class CANSocket : public iostream{
	private:
	struct canfd_frame cf;
	int can;
	struct msghdr msg;

	public:	
	CANSocket(string interface);
	~CANSocket();

	CANSocket& operator<<(Packet& packet);
	CANSocket& operator>>(Packet& packet);
};

void send_pkt(int can, const struct canfd_frame *cf, int mtu);
void receive_pkt(int can, struct canfd_frame *cf, int mtu);

#endif
