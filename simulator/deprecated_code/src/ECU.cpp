#include "ECU.h"

ECU::ECU(Protocol* p){
	protocol = p;
}

void ECU::set_bus(Bus* b){
	bus = b;
}

// spawn a thread to begin operating ECU
void ECU::start(){
	stop_ecu_thread = false;
	ecu_thread = new thread(&ECU::main, this);
}

// terminate the operating thread
void ECU::stop(){
	stop_ecu_thread = true;

	// wait for the thread to stop
	ecu_thread->join();
}

void ECU::main(){
	// clock which calls protocol->main() every tick
	auto last_time = Clock::now();

	while(!stop_ecu_thread){
		if(Clock::now() - last_time > chrono::nanoseconds(PERIOD)){
			protocol->main(bus);
			last_time = Clock::now();
		}else{
			this_thread::sleep_for(Clock::now() - last_time - chrono::nanoseconds(1000));	
		}
	}
}

Gateway::Gateway(Protocol* p, int sockets) : ECU(p){
	for(int i=0; i<sockets; i++){
		//TODO take in a list of CAN_INTERFACE names
		can_socket_vector.push_back(new CANSocket(CAN_INTERFACE));
	}
}

Gateway::~Gateway(){
	// deconstruct each CANSocket
	for(vector<CANSocket*>::iterator it=can_socket_vector.begin();
			it!=can_socket_vector.end(); ++it){
		delete *it;
	}
}

Node::Node(Protocol* p) : Gateway(p, 1){}
