#ifndef ECU_H
#define ECU_H

class Bus;
class Protocol;

#include <vector>
#include <chrono>
#include <thread>
#include "Protocol.h"
#include "CANSocket.h"

using namespace std;

/**
 * ECU, Clock, and Bus definitions
 * describe the architecture on which the protocols run
 * track metrics for evaluation of protocols
 * Clock is attached to ECU and 
 */

/**
 * The rational behind including the main method call inside a loop in the ECU goes as follows
 * Timing for the purposes of the Bus is on a message-by-message basis
 * The main loop is where each ECU decides what it wants to do.
 *
 * The ECU acts as on a different thread.
 */

class Clock : public std::chrono::high_resolution_clock{};

class ECU{
	// PERIOD determines how frequently the main() method of ECU runs
	//int PERIOD = 1000000000; // 1 s
	//int PERIOD = 500000000; // 500 ms
	int PERIOD = 1000000; // 1 ms
	//int PERIOD = 100000; // 100 us
	//int PERIOD = 1000; // 1 us

	Protocol* protocol;
	thread* ecu_thread;
	Bus* bus;
	bool stop_ecu_thread;

	public:
	ECU(Protocol* p);

	void set_bus(Bus* b);
	void start();
	void stop();
	void main();
};

/**
 * Gateway is a special type of ECU
 * it has rules for routing packets received
 * from one interface to another
 *
 * Gateways have several CANSockets
 */

class Gateway : public ECU{
	private:
	vector<CANSocket*> can_socket_vector;

	public:
	Gateway(Protocol* p, int sockets);
	~Gateway();
};

/**
 * Node is a type of Gateway with only one CANSocket
 */

class Node : public Gateway{
	public:
	Node(Protocol* p);
};

#endif
