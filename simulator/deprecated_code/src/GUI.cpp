#include "GUI.h"

GUIComponent::GUIComponent(){
	//TODO use a theme xml file to set these
	setOutlineColor(Color::Green);
	setOutlineThickness(10.0f);
	setFillColor(Color::Black);
}

// adds a subcomponent to this GUI element
void GUIComponent::add_component(GUIComponent *component){
	component_vector.push_back(component);
}

// draw all subcomponents of this component
// note: all components are drawn to the 'target' by their parent
void GUIComponent::draw(RenderTarget &target) const{
	// draw all subcomponents
	for(vector<GUIComponent*>::const_iterator it=component_vector.begin(); 
			it!=component_vector.end(); ++it){
		// draw subcomponent
		target.draw(**it);

		// call draw method of subcomponent 
		// (so it can draw all of it's subcomponents)
		(**it).draw(target);
	}
}

// handle event for this component
// distribute event to all subcomponents
void GUIComponent::event(Event e){
	for(vector<GUIComponent*>::iterator it=component_vector.begin(); 
			it!=component_vector.end(); ++it){
		(*it)->event(e);
	}
}

Terminal::Terminal() : GUIComponent(){
	setSize(Vector2f(600.0f,300.0f));
	setPosition(getOutlineThickness(),610.0f);
}

void Terminal::event(Event e){
	// handle event for this component
	if(e.type == Event::Resized) cout << e.text.unicode;
	//if(e.type == Event::KeyPressed) cout << e.key.code;
	//else if(e.type == Event::TextEntered); //TODO scale this object

	// distribute event to all subcomponents
	GUIComponent::event(e);
}

NodeDisplay::NodeDisplay() : GUIComponent(){
	setSize(Vector2f(600.0f,600.0f));
	setPosition(getOutlineThickness(),10.0f);
}

void NodeDisplay::event(Event e){
	// handle event for this component
	if(e.type == Event::Resized) cout << e.text.unicode;
	//if(e.type == Event::KeyPressed) cout << e.key.code;
	//else if(e.type == Event::TextEntered); //TODO scale this object

	// distribute event to all subcomponents
	GUIComponent::event(e);
}

void GUI::render_loop(){
	RenderWindow window(VideoMode(800, 600), "Securing CAN");

	while(window.isOpen()){
		// Detect if GUI::stop() has been called
		if(stop_gui_thread) window.close();

		// EVENT
		// each gui component will be given the event serially
		Event event;
		while (window.pollEvent(event)){
			if (event.type == sf::Event::Closed) window.close();

			gui_component.event(event);
		}


		// DRAWING
		window.clear();
		//window.draw(gui_component);
		gui_component.draw(window);
		window.display();	
	}
}

GUI::GUI(){
	//TODO load in all GUIComponents which will be used from XML
	//	have GUIComponents load their own subcomponents

	gui_component.add_component(new Terminal());
	gui_component.add_component(new NodeDisplay());
}

void GUI::start(){
	stop_gui_thread = false;
	gui_thread = new thread(&GUI::render_loop, this);
}

void GUI::stop(){
	stop_gui_thread = true;
	gui_thread->join();
}
