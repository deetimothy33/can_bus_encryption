#ifndef GUI_H
#define GUI_H

#include <iostream>
#include <vector>
#include <thread>
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

/**
 * GUIComponent can be drawn as part of the GUI
 * note: subcomponents of GUIComponents are also GUIComponents
 * 	this is an implicit tree of components
 */

class GUIComponent : public RectangleShape{
	protected:
	vector<GUIComponent*> component_vector;
	
	public:
	GUIComponent();

	// adds a subcomponent to this GUI element
	virtual void add_component(GUIComponent *component);

	// draw all subcomponents of this component
	// note: all components are drawn to the 'target' by their parent
	virtual void draw(RenderTarget &target) const;

	// handle event for this component
	// distribute event to all subcomponents
	virtual void event(Event e);		
};

/**
 * Terminal provides a command line interface to the user
 */

class Terminal : public GUIComponent{
	public:
	Terminal();

	virtual void event(Event e);
};

/**
 * NodeDisplay allows interaction with all nodes in the network
 */

class NodeDisplay : public GUIComponent{
	public:
	NodeDisplay();

	virtual void event(Event e);
};

// provides a view with controller
class ViewComponent : public GUIComponent, public View{
	public:

	virtual void event(Event e){
		// control the screen position?
	}
};

/**
 * GUI
 * display{
 * 	protocol selection
 * 	simulation selection
 * 	security metrics
 * 	performance metrics
 * 	metric printout
 * 	graphical depiction of bus
 * 	graphical depection of messages
 */

class GUI{
	private:
	GUIComponent gui_component;
	bool stop_gui_thread;
	thread* gui_thread;

	void render_loop();

	public:
	GUI();

	void start();
	void stop();
};

#endif
