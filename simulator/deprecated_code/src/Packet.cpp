#include "Packet.h"
	
BasePacket::BasePacket(){
	id = 0;
}

BasePacket::~BasePacket(){}

void BasePacket::encrypt(Key key){}
void BasePacket::decrypt(Key key){}

void BasePacket::sign(Key key){}
bool BasePacket::verify(Key key){ return true; }

CANAuthPacket::CANAuthPacket(){
	type = CAN;
}

SecurePacket::SecurePacket(){
	type = CANFD;
}

void SecurePacket::sign(Key key){
	// AES-CMAC(key, message || counter);
	// tag is the 32 LSB of the MAC
	secure_packet_data.tag = Crypto::mask(0,31) & Crypto::aes_cmac(key, secure_packet_data.mctr);
	//TODO make sure passing mctr works as expected. I could Crypto::mask it instead
}

bool SecurePacket::verify(Key key){
	// CMAC(key, secure_packet_data.mctr) == secure_packet_data.tag;
	// tag is the 32 LSB of the MAC
	return (Crypto::aes_cmac(key, secure_packet_data.mctr) & Crypto::mask(0,31)) == secure_packet_data.tag;;
}

CertificatePacket::CertificatePacket(){
	type = CANFD;
}
	
bool CertificatePacket::verify(Key key){
	// ED25519(key, certificate_packet_data.certificate) == certificate_packet_data.signature;
	return Crypto::ed25519(key, certificate_packet_data.certificate) == certificate_packet_data.signature;
}

KeyDistributionPacket::KeyDistributionPacket(){
	type = CANFD;
}

void KeyDistributionPacket::encrypt(Key key){
	// CURVE25519-ENCRYPT(key, *data); 
	*data = Crypto::curve25519_encrypt(key, *data);
	//TODO does this assignment work?
	//	perhaps I will need to do each byte individually?
}

void KeyDistributionPacket::decrypt(Key key){
	// CURVE25519-DECRYPT(key, *data); 
	*data = Crypto::curve25519_decrypt(key, *data);
	//TODO same assignment question
}
