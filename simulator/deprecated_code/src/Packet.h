#ifndef PACKET_H
#define PACKET_H

using namespace std;

#include <linux/can.h>
#include "Crypto.h"

enum PacketType { CAN, CANFD };

/**
 * Packets are transmitted on the CANbus
 * different packets exist for different types of CAN protocols (CAN-FD, CAN...)
 *
 * This is a wrapper around 
 * 	can_frame
 * 	canfd_frame
 * defined in /include/uapi/linux/can.h
 */

class Packet{
	public:
	PacketType type;

	// id's are a property of the packet / message
	uint32_t id;
	uint8_t data_length;
	uint8_t flags; // padding for normal can_frame, flags for canfd_frame
	uint8_t reserved_0;
	uint8_t reserved_1;
	unsigned char data[CANFD_MAX_DLEN];
	
	virtual ~Packet() =0;

	// encrypt or decrypt (the current data bits)
	virtual void encrypt(Key key) =0;
	virtual void decrypt(Key key) =0;

	// sign or verify the message data (modifies the data field)
	virtual void sign(Key key) =0;
	virtual bool verify(Key key) =0;
};	
inline Packet::~Packet(){}

class BasePacket : public Packet{
	public:
	BasePacket();
	virtual ~BasePacket();

	virtual void encrypt(Key key);
	virtual void decrypt(Key key);
	
	virtual void sign(Key key);
	virtual bool verify(Key key);
};

class CANAuthPacket : public BasePacket{
	public:
	CANAuthPacket();
};

union SecurePacketData{
	uint128_t data;
	struct{
		uint64_t message;
		uint32_t counter;
		uint32_t tag;
	};
	struct{
		//TODO will this do what I think it does?
		//	namely, will accessing this get the first 96 bits of data?
		uint128_t mctr : 96;
	};
};

class SecurePacket : public BasePacket{
	SecurePacketData secure_packet_data;
	
	public:
	SecurePacket();
	
	virtual void sign(Key key);
	virtual bool verify(Key key);
};

//TODO update this to be useful
//TODO the bitfields do not necessarily reflect X.509 certificates yet
union CertificatePacketData{
	uint128_t data;
	//TODO does this do what I think it does? 
	//	I want the first 96 bits to be the certificate
	//	the last 32 bits to be the signature
	struct{
		uint128_t certificate : 96;
		uint32_t signature; 
	};
	//TODO update to reflect the location of the public key
	struct{
		uint64_t junk;
		uint32_t public_key;
	};
};

class CertificatePacket : public BasePacket{
	CertificatePacketData certificate_packet_data;

	public:
	CertificatePacket();
	
	virtual bool verify(Key key);	
};

class KeyDistributionPacket : public BasePacket{
	public:
	KeyDistributionPacket();
	
	virtual void encrypt(Key key);
	virtual void decrypt(Key key);
};

class KeyNegotiationPacket: public BasePacket{
	public:
	KeyNegotiationPacket();
	
	virtual void encrypt(Key key);
	virtual void decrypt(Key key);
};

#endif
