#include "Protocol.h"

BaseProtocol::BaseProtocol(){
	is_joined = false;
	has_key = false;
	has_freshness = false;

	//TODO test packet
	CANAuthPacket* p = new CANAuthPacket();
	p->id = rand() % 1024; // TODO this may not actually access what I think it is accessing
	p->data[0] = 1;
	p->data[1] = 2;
	send_queue.push(p);
}

//TODO may need to change some of the condtions on which nodes do things

// common parts of main function, subclasses can call 'super'.main()
void BaseProtocol::main(Bus* bus){
	// describe the ECU behavior
	// sending takes priority over processing (right now)
	if(!is_joined){
		// join the bus if it has not been joined
		is_joined = true;
		join_bus();
	}else if(!has_key){
		has_key = true;
		negotiate_session_key();
	}else if(!has_freshness){
		has_freshness = true;
		negotiate_freshness();
	}else if(!send_queue.empty()){
		// if there are messages to transmit, transmit them
		// other behaviors will add messages to the queue
		if(transmit(send_queue.front(), bus)){
			delete send_queue.front();
			send_queue.pop();
		}
	}else if(!receive_queue.empty()){
		// process received packet
		process(receive_queue.front());
		delete receive_queue.front();
		receive_queue.pop();
	}else if(bus->sync()){
		// if bus has entered the sync phase,
		// then wait to receive a packet
		Packet* p = new CANAuthPacket();
		receive(p, bus);
		receive_queue.push(p);
	}
	//TODO also implement detection scenarios (as behaviors)?	
}

bool BaseProtocol::transmit(Packet* packet, Bus* bus){
	// tell other nodes to receive
	//bus->sync(true);
	//this_thread::sleep_for(chrono::milliseconds(100));
	//bus->sync(false);

	*bus << *packet;

	cout << "tx" << endl;

	return true;
};

// add any packet transmitted on the bus to the receive_queue
void BaseProtocol::receive(Packet* packet, Bus* bus){
	// wait for sync to finish
	//while(bus->sync()) this_thread::sleep_for(chrono::microseconds(10));

	*bus >> *packet;

	//cout << "id:" << *(packet->id) << endl;
	cout << "data:" << (packet->data[0]) << endl;
};

void BaseProtocol::process(Packet* packet){
	// process a received packet:
	// determine if the packet requires any behavior from this ECU
	//TODO
	
	// add any packet ECU needs to send to the send_queue
	//TODO
};

void BaseProtocol::join_bus(){};
void BaseProtocol::leave_bus(){};
void BaseProtocol::update_freshness(){};
void BaseProtocol::negotiate_freshness(){};
void BaseProtocol::negotiate_session_key(){};
void BaseProtocol::negotiate_master(){};
void BaseProtocol::authenticate_ECU(){};

void BaseProtocol::detect_power_up(){};
void BaseProtocol::detect_ECU_join(){};
void BaseProtocol::detect_ECU_leave(){};
void BaseProtocol::detect_ECU_disconnect(){};
void BaseProtocol::detect_ECU_out_of_sync(){};
void BaseProtocol::detect_ECU_far_future_freshness(){};
void BaseProtocol::detect_choose_master(){};

XMLProtocol::XMLProtocol(string xml_filename) : BaseProtocol(){
	load_protocol_xml(xml_filename);
}

void XMLProtocol::main(Bus* bus){
	BaseProtocol::main(bus);
	//TODO
}

void XMLProtocol::join_bus(){
	//TODO
}

void XMLProtocol::load_protocol_xml(string filename){
	XMLError error;
	XMLDocument xml;
	XMLNode *root_node;
	XMLElement *element;

	// load file
	error = xml.LoadFile(filename.c_str());
	XMLCheckResult(error);
	
	// get the root node
	root_node = xml.FirstChild();
	NULLCHECK(root_node);

	// extract topology, add ECU to the bus
	element = root_node->FirstChildElement("topology");
	NULLCHECK(element);

	//TODO
}

void CenteralizedProtocol::main(Bus* bus){
	BaseProtocol::main(bus);
	//TODO
}

void CenteralizedProtocol::join_bus(){
}

void DistributedProtocol::main(Bus* bus){
	BaseProtocol::main(bus);
	//TODO
}

void DistributedProtocol::join_bus(){
}

