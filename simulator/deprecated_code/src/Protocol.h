#ifndef PROTOCOL_H
#define PROTOCOL_H

class Packet;
class Bus;

#include <iostream>
#include <queue>
#include <chrono>
#include <thread>
#include "Bus.h"
#include "Packet.h"
#include "tinyxml2.h"
#include "macro.h"

using namespace std;
using namespace tinyxml2;

/**
 * Protocols define how ECU operates.
 * The purpose of doing it this way is to make the protocols used clear and easy to read.
 * In construction CAN bus, passing different protocols to ECU will change how they act.
 *
 * The protocols can also be broken down into small peices.
 * This prevents duplication of code bits.
 * Protocols only need to subclass the BaseProtocol and implement the portions needed.
 */

class Protocol{
	public:
	// function will be executed in a loop by ECUs
	// main describes the behavior of the ECU
	// behavior := when to call the other functions defined by the protocol
	virtual void main(Bus*) =0;

	// true if transmission was successful
	virtual bool transmit(Packet* packet, Bus* bus) =0;
	virtual void receive(Packet* packet, Bus* bus) =0;
	virtual void process(Packet* packet) =0;

	virtual void join_bus() =0;
	virtual void leave_bus() =0;
	virtual void update_freshness() =0;
	virtual void negotiate_freshness() =0;
	virtual void negotiate_session_key() =0;
	virtual void negotiate_master() =0;
	virtual void authenticate_ECU() =0;

	// network is powering up
	virtual void detect_power_up() =0;
	// an ECU joins the network
	virtual void detect_ECU_join() =0;
	// an ECU leaves (or is removed) the network
	virtual void detect_ECU_leave() =0;
	// an ECU disconnects from the network (may be the same as leaving)
	virtual void detect_ECU_disconnect() =0;
	virtual void detect_ECU_out_of_sync() =0;
	virtual void detect_ECU_far_future_freshness() =0;
	// determine when a new master must be chosen
	virtual void detect_choose_master() =0;
};

// things common for Centeralized and Distributed Protocols
class BaseProtocol : public Protocol{
	bool is_joined;
	bool has_key;
	bool has_freshness;
	queue<Packet*> send_queue;
	queue<Packet*> receive_queue;

	public:
	BaseProtocol();

	// common parts of main function, subclasses can call 'super'.main()
	virtual void main(Bus* bus);	

	virtual bool transmit(Packet* packet, Bus* bus);
	virtual void receive(Packet* packet, Bus* bus);
	virtual void process(Packet* packet);
	
	virtual void join_bus();
	virtual void leave_bus();
	virtual void update_freshness();
	virtual void negotiate_freshness();
	virtual void negotiate_session_key();
	virtual void negotiate_master();
	virtual void authenticate_ECU();

	virtual void detect_power_up();
	virtual void detect_ECU_join();
	virtual void detect_ECU_leave();
	virtual void detect_ECU_disconnect();
	virtual void detect_ECU_out_of_sync();
	virtual void detect_ECU_far_future_freshness();
	virtual void detect_choose_master();
};

// XML defines behavior of node
class XMLProtocol : public BaseProtocol{
	private:	
	void load_protocol_xml(string filename);

	public:
	XMLProtocol(string xml_filename);

	virtual void main(Bus* bus);
	virtual void join_bus();	
};

// common for all centeralized protocols
class CenteralizedProtocol : public BaseProtocol{
	virtual void main(Bus* bus);
	virtual void join_bus();	
};

// common for all distributed protocols
class DistributedProtocol : public BaseProtocol{
	virtual void main(Bus* bus);
	virtual void join_bus();
};

// a protocol for Malfunctioning nodes
class MalfunctionProtocol : public BaseProtocol{};

class C1 : public CenteralizedProtocol{

};

class D1 : public DistributedProtocol{

};

class M1 : public MalfunctionProtocol{

};

#endif
