#include "Simulation.h"

//TODO perhaps XML file should define <event time=n action=m/>
//	(event happens at time n causing action m)

Simulation::Simulation(Bus* b){
	bus = b;
}

void Simulation::print_metrics(){
	// print all possible metrics from the bus
	cout << "-------------" << endl;
	cout << bus->metric << endl;
}
	
void XMLSimulation::load_simulation_xml(string filename){
	XMLError error;
	XMLDocument xml;
	XMLNode *root_node;
	//XMLElement *element;

	// load file
	error = xml.LoadFile(filename.c_str());
	XMLCheckResult(error);
	
	// get the root node
	root_node = xml.FirstChild();
	NULLCHECK(root_node);

	// extract topology, add ECU to the bus
	//element = root_node->FirstChildElement("topology");
	//NULLCHECK(element);
	//TODO
}

XMLSimulation::XMLSimulation(Bus* b, string xml_filename) : Simulation(b){
	load_simulation_xml(xml_filename);
}

void XMLSimulation::run(){
	bus->power_up();
	//TODO make this time be a parameter loaded from the XML file
	this_thread::sleep_for(chrono::seconds(4));
	bus->power_down();
}

TestSimulation::TestSimulation(Bus* b) : Simulation(b){}

void TestSimulation::run(){
	bus->attach(new ECU(new C1()));
	bus->attach(new ECU(new C1()));

	bus->power_up();
	this_thread::sleep_for(chrono::seconds(4));
	bus->power_down();
}
