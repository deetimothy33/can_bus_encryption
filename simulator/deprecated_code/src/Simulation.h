#ifndef SIMULATION_H
#define SIMULATION_H

#include <iostream>
#include <chrono>
#include "Bus.h"
#include "tinyxml2.h"
#include "macro.h"

using namespace std;
using namespace tinyxml2;

/**
 * Simulations
 * Run a preset scenario on the given bus
 */

/**
 * Simulations are defined in XML files
 * These files define
 * 	1. the topology of the bus
 * 	2. protocols used by nodes on the bus
 * 		(but not the protocols themselves)
 * 	3. parameters of simulation
 * 		length of simulation in time
 * 	4. any events to happen during simulation
 * 		such as node failure
 * 		a potential attack by an adversary
 */

class Simulation{	
	protected:
	Bus* bus;

	public:
	Simulation(Bus* b);

	void print_metrics();

	virtual void run() =0;
};

class XMLSimulation : public Simulation{
	private:
	void load_simulation_xml(string filename);
	
	public:
	XMLSimulation(Bus* b, string xml_filename);

	void run();
};

class TestSimulation : public Simulation{
	public:
	TestSimulation(Bus* b);

	void run();
};

#endif
