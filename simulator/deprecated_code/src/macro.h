#ifndef MACRO_H
#define MACRO_H

#include <iostream>
#include "tinyxml2.h"

using namespace std;

#define CAN_INTERFACE "vcan0"

#define ERROR(string) cerr << "ERROR: " << string << endl;
#define PRINT(string) cout << string << endl;

#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) printf("Error: %i\n", a_eResult);
#define NULLCHECK(element) if(element == nullptr){ ERROR("element has no existence"); return; }

#endif
