#include <iostream>
#include <string>
#include "Bus.h"
#include "ECU.h"
#include "Protocol.h"
#include "Packet.h"
#include "Simulation.h"
#include "GUI.h"

using namespace std;

//TODO how to transmit Packet on CANSocket belonging to an ECU
//	instead of ECU having a Protocol, mabe Protocol can have an ECU?
//	***ECU can transmit things from Protocol send_queue***
//		could then move interfacing with the bus entirely to ECU
//	**Protocol has an ECU instead of a bus
//		ECU is the thing it needs to interact with anyway

//TODO
// make a minimalist sample
// using the underlying SocketCAN interface I created
// does the test code already have one?
//TODO

/**
 * BASIC DESIGN
 * Node are connected by Bus
 * Bus are connected by Gateway
 * All gateways are grouped into a master Bus
 */

/**
 * main
 * create a bus
 * run simulation on that bus
 *
 * ***LATER: start GUI
 * 	GUI will be done if there is time
 */

int main(){
	GUI gui;
	return 0;

	Bus bus;
	XMLSimulation simulation(&bus, "xml/simulation/flat.xml");

	cout << "Instantiating Protocols" << endl;
	cout << "Creating CAN Bus" << endl;
	cout << "Running Simulation" << endl;
	simulation.run();

	cout << "Printing Metrics" << endl;
	simulation.print_metrics();

	//TODO perhaps I could give the bus to many different simulations (for testing)
	// these simulations could cause nodes to be added and removed ect.
	// Or I could create a run method on the bus that would run something of Simulation class

	return 0;
}

/**
 * notes
 * includes: section 4, http://www.cplusplus.com/forum/articles/10627/
 * crypto++565 library for cryptography
 * TinyXML-2 for XML parsing
 */

/**
 * related projects
 * https://github.com/linux-can/can-utils
 * https://github.com/zombieCraig/ICSim 
 */

/**
 * utilizes SocketCAN a set of open source CAN drivers and a networking stack in the Linux kernel.
 * can-utils package provides this.
 * can-utils utilizes SocketCAN
 *
 * SocketCAN is the offical CAN API of the linux kernel
 */

//TODO GOAL: get the ECU transmitting, receiving messages without encryption (done)
//TODO GOAL: Now handle the encryption part
//
//TODO make sure message transmission is working
//TODO make sure message transmission works with incrased speed
//TODO speeding up buss transmissions after arbitration
//
//TODO handle busses which are not flat
//	=> they have relays
//
//TODO update Crypto.cpp
//
//TODO define protocols for 
//	key negotiation
//	key distribution
//	freshness synchronization
//	scenario detection
//
//TODO completly define simulations in XML
//	make a folder ( xml/simulation/ ) that defines all simulations to be done
//	main can then iterate over all files in this folder to run the simulations
//
//TODO GUI can also be defined in XML?
//
//TODO Add Gateways which work similarly to ECU.
//	could be a subclass of ECU
//	perhaps ECU can have two subclasses
//		1. node -- connect to other nodes and gateway (can bus)
//		2. gateway -- connect to backbone (ethernet?) and group of nodes (can bus?)
//
//TODO change communications such that we don't worry about bit-level stuff
//	just give a message to the transceiver in order to have it sent on the bus
//
//TODO main() receives simulation file and XML file as arguments?
//	alternatively, the user could choose them in the gui
//
//TODO CANSocket
//
//TODO could start all GUI compononets on separate threads 
//	(if some of them do a lot of processing this will be good)
//
//TODO perhaps I should define attacks in XML too.
//	then I can specifiy which attacks to try on a given simulation
//
//TODO controls.c 454 stat(traffic_log) => this is useful
//		522 bg_player injects background traffic
//
//TODO library for X.509 certificate parsing
