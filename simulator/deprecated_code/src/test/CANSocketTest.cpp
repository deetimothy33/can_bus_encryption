#include "gtest/gtest.h"

#include <thread>
#include "Packet.h"
#include "CANSocket.h"

// try to use CANSocket to send and receive a message
CANSocket can_socket_s(CAN_INTERFACE);
CANSocket can_socket_r(CAN_INTERFACE);
CANAuthPacket can_p = CANAuthPacket();
SecurePacket canfd_p = SecurePacket();
void r() { can_socket_r >> can_p; }
void rfd() { can_socket_r >> canfd_p; }

TEST(CANSocket, CanPacket){
	// CAN packet
	CANAuthPacket can_p_s = CANAuthPacket();
	
	thread t(r);
	sleep(1);

	// put some data in packet
	can_p_s.id = 10;
	can_p_s.data_length = 1;
	can_p_s.flags = 1; 
	can_p_s.reserved_0 = 1;
	can_p_s.reserved_1 = 1;
	can_p_s.data[0] = 80;

	// send the packet
	can_socket_s << can_p_s;

	// wait for receiver to finish reading
	t.join();

	// do I receive the same information I sent?
	EXPECT_EQ(can_p.id, can_p_s.id);
	EXPECT_EQ(can_p.data_length, can_p_s.data_length);
	EXPECT_EQ(can_p.flags, can_p_s.flags);
	EXPECT_EQ(can_p.reserved_0, can_p_s.reserved_0);
	EXPECT_EQ(can_p.reserved_1, can_p_s.reserved_1);
	EXPECT_EQ(can_p.data[0], can_p_s.data[0]);
}

TEST(CANSocket, CanfdPacket){
	// CANFD packet
	SecurePacket canfd_p_s = SecurePacket();
	
	thread tfd(rfd);
	sleep(1);

	// put some data in packet
	canfd_p_s.id = 10;
	canfd_p_s.data_length = 1;
	canfd_p_s.flags = 1; 
	canfd_p_s.reserved_0 = 1;
	canfd_p_s.reserved_1 = 1;
	canfd_p_s.data[0] = 80;

	// send the packet
	can_socket_s << canfd_p_s;

	// wait for receiver to finish reading
	tfd.join();

	// do I receive the same information I sent?
	EXPECT_EQ(canfd_p.id, canfd_p_s.id);
	EXPECT_EQ(canfd_p.data_length, canfd_p_s.data_length);
	EXPECT_EQ(canfd_p.flags, canfd_p_s.flags);
	EXPECT_EQ(canfd_p.reserved_0, canfd_p_s.reserved_0);
	EXPECT_EQ(canfd_p.reserved_1, canfd_p_s.reserved_1);
	EXPECT_EQ(canfd_p.data[0], canfd_p_s.data[0]);
}

